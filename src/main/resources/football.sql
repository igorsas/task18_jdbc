-- -----------------------------------------------------
-- Schema football_team
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `football_team` DEFAULT CHARACTER SET utf8;
USE `football_team`;

-- -----------------------------------------------------
-- Table `football_team`.`city`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `football_team`.`city`
(
  `id`   INT         NOT NULL PRIMARY KEY unique auto_increment,
  `name` VARCHAR(45) NOT NULL
)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `football_team`.`football_team`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `football_team`.`football_team`
(
  `id`      INT         NOT NULL UNIQUE,
  `name`    VARCHAR(45) NOT NULL,
  `age`     INT         NOT NULL,
  `city_id` INT         NOT NULL,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  PRIMARY KEY (`id`),
  INDEX `fk_football_team_city1_idx` (`city_id` ASC),
  CONSTRAINT `fk_football_team_city1`
    FOREIGN KEY (`city_id`)
      REFERENCES `football_team`.`city` (`id`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `football_team`.`position`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `football_team`.`position`
(
  `id`   INT         NOT NULL PRIMARY KEY unique auto_increment,
  `name` VARCHAR(45) NOT NULL
)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `football_team`.`footballer`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `football_team`.`footballer`
(
  `id`               INT         NOT NULL PRIMARY KEY UNIQUE auto_increment,
  `full_name`        VARCHAR(45) NOT NULL,
  `age`              INT         NOT NULL,
  `markert_price`    INT         NOT NULL,
  `salary`           INT         NOT NULL,
  `football_team_id` INT         NOT NULL,
  `position_id`      INT         NOT NULL,
  INDEX `fk_footballer_football_team1_idx` (`football_team_id` ASC),
  INDEX `fk_footballer_position1_idx` (`position_id` ASC),
  CONSTRAINT `fk_footballer_football_team1`
    FOREIGN KEY (`football_team_id`)
      REFERENCES `football_team`.`football_team` (`id`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  CONSTRAINT `fk_footballer_position1`
    FOREIGN KEY (`position_id`)
      REFERENCES `football_team`.`position` (`id`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `football_team`.`competition`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `football_team`.`competition`
(
  `id`         INT         NOT NULL PRIMARY KEY UNIQUE auto_increment,
  `prize_fund` INT         NOT NULL,
  `name`       VARCHAR(45) NOT NULL
)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `football_team`.`football_team_has_competition`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `football_team`.`football_team_has_competition`
(
  `football_team_id` INT NOT NULL,
  `competition_id`   INT NOT NULL,
  `amount_of_win`    INT NULL DEFAULT NULL,
  PRIMARY KEY (`football_team_id`, `competition_id`),
  INDEX `fk_football_team_has_competition_competition1_idx` (`competition_id` ASC),
  INDEX `fk_football_team_has_competition_football_team_idx` (`football_team_id` ASC),
  CONSTRAINT `fk_football_team_has_competition_football_team`
    FOREIGN KEY (`football_team_id`)
      REFERENCES `football_team`.`football_team` (`id`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  CONSTRAINT `fk_football_team_has_competition_competition1`
    FOREIGN KEY (`competition_id`)
      REFERENCES `football_team`.`competition` (`id`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
)
  ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `football_team`.`coach`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `football_team`.`coach`
(
  `id`               INT         NOT NULL PRIMARY KEY unique,
  `name`             VARCHAR(45) NOT NULL,
  `salary`           INT         NOT NULL,
  `football_team_id` INT         NOT NULL,
  INDEX `fk_coach_football_team1_idx` (`football_team_id` ASC),
  CONSTRAINT `fk_coach_football_team1`
    FOREIGN KEY (`football_team_id`)
      REFERENCES `football_team`.`football_team` (`id`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
)
  ENGINE = InnoDB;

-- -------
-- inserts
-- -------

insert into `football_team`.`city`
values (1, 'Barcelona'),
       (2, 'Manchester');

insert into `football_team`.`football_team`
values (1, 'FC Barcelona', 120, 1),
       (2, 'FC Manchester City', 125, 2);

insert into `football_team`.`position`
values (1, 'goalkeeper'),
       (2, 'defender'),
       (3, 'half_back'),
       (4, 'striker');

insert into `football_team`.`footballer`
values (1, 'Marc-André ter Stegen', 27, 40000000, 10000000, 1, 1),
       (2, 'Nélson Semedo', 26, 50000000, 8000000, 1, 2),
       (3, 'Gerard Piqué', 32, 70000000, 15000000, 1, 2),
       (4, 'Samuel Umtiti', 27, 25000000, 6000000, 1, 2),
       (5, 'Jordi Alba', 30, 40000000, 10000000, 1, 2),
       (6, 'Ivan Rakitic', 31, 35000000, 15000000, 1, 3),
       (7, 'Sergio Busquets', 31, 20000000, 12000000, 1, 3),
       (8, 'Coutinho', 27, 110000000, 25000000, 1, 3),
       (9, 'Arturo Vidal', 32, 80000000, 20000000, 1, 3),
       (10, 'Lionel Messi', 32, 100000000, 35000000, 1, 4),
       (11, 'Luis Suárez', 32, 45000000, 25000000, 1, 4),
       (12, 'EDERSON MORAES', 26, 90000000, 15000000, 2, 1),
       (13, 'KYLE WALKER', 29, 65000000, 20000000, 2, 2),
       (14, 'VINCENT KOMPANY', 33, 15000000, 20000000, 2, 2),
       (15, 'NICOLAS OTAMENDI', 31, 30000000, 15000000, 2, 2),
       (16, 'OLEKSANDR ZINCHENKO', 22, 30000000, 5000000, 2, 2),
       (17, 'DAVID SILVA', 33, 40000000, 20000000, 2, 3),
       (18, 'KEVIN DE BRUYNE', 28, 120000000, 25000000, 2, 3),
       (19, 'FERNANDINHO LUIZ ROZA', 34, 15000000, 25000000, 2, 3),
       (20, 'GABRIEL JESUS', 21, 75000000, 10000000, 2, 4),
       (21, 'LEROY SANE', 22, 80000000, 14000000, 2, 4),
       (22, 'SERGIO AGUERO', 31, 50000000, 18000000, 2, 4);

insert into `football_team`.`competition`
values (1, 200000000, 'Champion`s League'),
       (2, 150000000, 'Barclays Premier League'),
       (3, 80000000, 'La-Liga');

insert into `football_team`.`football_team_has_competition`
values (1, 1, 5),
       (1, 3, 25),
       (2, 2, 3);

insert into `football_team`.`coach`
values (1, 'Ernesto Valverde', 13000000, 1),
       (2, 'Josep Guardiola', 21000000, 2);
