package com.igor.model.entity;

import com.igor.model.annotation.Column;
import com.igor.model.annotation.PrimaryKey;
import com.igor.model.annotation.Table;

import static com.igor.model.constant.FootballerConst.*;

@Table(name = TABLE_NAME)
public class FootballerEntity {
    @PrimaryKey
    @Column(name = ID)
    private int id;
    @Column(name = FULL_NAME)
    private String fullName;
    @Column(name = AGE)
    private int age;
    @Column(name = MARKET_PRICE)
    private long marketPrice;
    @Column(name = SALARY)
    private long salary;
    @Column(name = FOOTBALL_TEAM_ID)
    private int footballTeamId;
    @Column(name = POSITION_ID)
    private int positionId;

    public FootballerEntity() {
    }

    public FootballerEntity(final int id, final String fullName, final int age, final long marketPrice,
                            final long salary, final int footballTeamId, final int positionId) {
        this.id = id;
        this.fullName = fullName;
        this.age = age;
        this.marketPrice = marketPrice;
        this.salary = salary;
        this.footballTeamId = footballTeamId;
        this.positionId = positionId;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(final String fullName) {
        this.fullName = fullName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(final int age) {
        this.age = age;
    }

    public long getMarketPrice() {
        return marketPrice;
    }

    public void setMarketPrice(final long marketPrice) {
        this.marketPrice = marketPrice;
    }

    public long getSalary() {
        return salary;
    }

    public void setSalary(final long salary) {
        this.salary = salary;
    }

    public int getFootballTeamId() {
        return footballTeamId;
    }

    public void setFootballTeamId(final int footballTeamId) {
        this.footballTeamId = footballTeamId;
    }

    public int getPositionId() {
        return positionId;
    }

    public void setPositionId(final int positionId) {
        this.positionId = positionId;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s %s %s %s %s",
                id, fullName, age, marketPrice, salary, footballTeamId, positionId);
    }
}
