package com.igor.model.entity;

import com.igor.model.annotation.Column;

import static com.igor.model.constant.FootballTeamHasCompetitionConst.COMPETITION_ID;
import static com.igor.model.constant.FootballTeamHasCompetitionConst.FOOTBALL_TEAM_ID;

public class PrimaryKeyFootballCompetition {
    @Column(name = FOOTBALL_TEAM_ID)
    private int footballTeamId;
    @Column(name = COMPETITION_ID)
    private int competitionId;

    public PrimaryKeyFootballCompetition() {
    }

    public PrimaryKeyFootballCompetition(final int footballTeamId, final int competitionId) {
        this.footballTeamId = footballTeamId;
        this.competitionId = competitionId;
    }

    public int getFootballTeamId() {
        return footballTeamId;
    }

    public void setFootballTeamId(final int footballTeamId) {
        this.footballTeamId = footballTeamId;
    }

    public int getCompetitionId() {
        return competitionId;
    }

    public void setCompetitionId(final int competitionId) {
        this.competitionId = competitionId;
    }

    @Override
    public String toString() {
        return String.format("%s %s", footballTeamId, competitionId);
    }
}
