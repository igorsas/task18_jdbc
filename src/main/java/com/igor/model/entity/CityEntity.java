package com.igor.model.entity;

import com.igor.model.annotation.Column;
import com.igor.model.annotation.PrimaryKey;
import com.igor.model.annotation.Table;

import static com.igor.model.constant.CityConst.*;

@Table(name = TABLE_NAME)
public class CityEntity {
    @PrimaryKey
    @Column(name = ID)
    private int id;
    @Column(name = NAME, length = 20)
    private String name;

    public CityEntity() {
    }

    public CityEntity(final int id, final String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%s %s", id, name);
    }
}
