package com.igor.model.entity;

import com.igor.model.annotation.Column;
import com.igor.model.annotation.PrimaryKey;
import com.igor.model.annotation.Table;

import static com.igor.model.constant.CoachConst.*;

@Table(name = TABLE_NAME)
public class CoachEntity {
    @PrimaryKey
    @Column(name = ID)
    private int id;
    @Column(name = NAME)
    private String name;
    @Column(name = SALARY)
    private long salary;
    @Column(name = FOOTBALL_TEAM_ID)
    private int footballTeamId;

    public CoachEntity() {
    }

    public CoachEntity(final int id, final String name, final long salary, final int footballTeamId) {
        this.id = id;
        this.name = name;
        this.salary = salary;
        this.footballTeamId = footballTeamId;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public long getSalary() {
        return salary;
    }

    public void setSalary(final long salary) {
        this.salary = salary;
    }

    public int getFootballTeamId() {
        return footballTeamId;
    }

    public void setFootballTeamId(final int footballTeamId) {
        this.footballTeamId = footballTeamId;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s %s", id, name, salary, footballTeamId);
    }
}
