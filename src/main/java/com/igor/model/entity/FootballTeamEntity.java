package com.igor.model.entity;

import com.igor.model.annotation.Column;
import com.igor.model.annotation.PrimaryKey;
import com.igor.model.annotation.Table;

import static com.igor.model.constant.FootballTeamConst.*;

@Table(name = TABLE_NAME)
public class FootballTeamEntity {
    @PrimaryKey
    @Column(name = ID)
    private int id;
    @Column(name = NAME)
    private String name;
    @Column(name = AGE)
    private int age;
    @Column(name = CITY_ID)
    private int cityId;

    public FootballTeamEntity() {
    }

    public FootballTeamEntity(final int id, final String name, final int age, final int cityId) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.cityId = cityId;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(final int age) {
        this.age = age;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(final int cityId) {
        this.cityId = cityId;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s %s", id, name, age, cityId);
    }
}
