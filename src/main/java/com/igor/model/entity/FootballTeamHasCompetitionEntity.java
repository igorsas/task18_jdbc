package com.igor.model.entity;


import com.igor.model.annotation.Column;
import com.igor.model.annotation.PrimaryKeyComposite;
import com.igor.model.annotation.Table;

import static com.igor.model.constant.FootballTeamHasCompetitionConst.AMOUNT_OF_WIN;
import static com.igor.model.constant.FootballTeamHasCompetitionConst.TABLE_NAME;

@Table(name = TABLE_NAME)
public class FootballTeamHasCompetitionEntity {
    @PrimaryKeyComposite
    private PrimaryKeyFootballCompetition primaryKey;
    @Column(name = AMOUNT_OF_WIN)
    private int amountOfWin;

    public FootballTeamHasCompetitionEntity() {
    }

    public FootballTeamHasCompetitionEntity(final PrimaryKeyFootballCompetition primaryKey, final int amountOfWin) {
        this.primaryKey = primaryKey;
        this.amountOfWin = amountOfWin;
    }

    public PrimaryKeyFootballCompetition getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(PrimaryKeyFootballCompetition primaryKey) {
        this.primaryKey = primaryKey;
    }

    public int getAmountOfWin() {
        return amountOfWin;
    }

    public void setAmountOfWin(final int amountOfWin) {
        this.amountOfWin = amountOfWin;
    }

    @Override
    public String toString() {
        return String.format("%s %s", primaryKey.toString(), amountOfWin);
    }
}
