package com.igor.model.entity;

import com.igor.model.annotation.Column;
import com.igor.model.annotation.PrimaryKey;
import com.igor.model.annotation.Table;

import static com.igor.model.constant.CompetitionConst.*;

@Table(name = TABLE_NAME)
public class CompetitionEntity {
    @PrimaryKey
    @Column(name = ID)
    private int id;
    @Column(name = PRIZE_FUND)
    private long prizeFund;
    @Column(name = NAME)
    private String name;

    public CompetitionEntity() {
    }

    public CompetitionEntity(final int id, final long prizeFund, final String name) {
        this.id = id;
        this.prizeFund = prizeFund;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(final int id) {
        this.id = id;
    }

    public long getPrizeFund() {
        return prizeFund;
    }

    public void setPrizeFund(final long prizeFund) {
        this.prizeFund = prizeFund;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%s %s %s", id, prizeFund, name);
    }
}
