package com.igor.model.constant;

public final class FootballTeamConst {
    public static final String TABLE_NAME = "football_team";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String AGE = "age";
    public static final String CITY_ID = "city_id";

    private FootballTeamConst() {
    }
}
