package com.igor.model.constant;

public final class MetaDataConst {
    public static final String TABLE = "TABLE";
    public static final String TABLE_NAME = "TABLE_NAME";
    public static final String COLUMN_NAME = "COLUMN_NAME";
    public static final String TYPE_NAME = "TYPE_NAME";
    public static final String COLUMN_SIZE = "COLUMN_SIZE";
    public static final String DECIMAL_DIGITS = "DECIMAL_DIGITS";
    public static final String IS_NULLABLE = "IS_NULLABLE";
    public static final String IS_AUTOINCREMENT = "IS_AUTOINCREMENT";
    public static final String FK_COLUMN_NAME = "FKCOLUMN_NAME";
    public static final String PK_TABLE_NAME = "PKTABLE_NAME";
    public static final String PK_COLUMN_NAME = "PKCOLUMN_NAME";
    public static final String YES = "YES";
    public static final String COLUMN_NAME_PATTERN = "%";
    public static final String FK_TO_STRING = "FK_COLUMN: %s,  PK_TABLE: %s,  PK_COLUMN_NAME: %s";

    private MetaDataConst() {
    }
}
