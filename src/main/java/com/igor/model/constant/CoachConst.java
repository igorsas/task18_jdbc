package com.igor.model.constant;

public final class CoachConst {
    public static final String TABLE_NAME = "coach";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String SALARY = "salary";
    public static final String FOOTBALL_TEAM_ID = "football_team_id";

    private CoachConst() {
    }
}
