package com.igor.model.constant;

public final class CompetitionConst {
    public static final String TABLE_NAME = "competition";
    public static final String ID = "id";
    public static final String PRIZE_FUND = "prize_fund";
    public static final String NAME = "name";

    private CompetitionConst() {
    }
}
