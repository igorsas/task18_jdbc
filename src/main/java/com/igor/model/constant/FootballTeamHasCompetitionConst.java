package com.igor.model.constant;

public final class FootballTeamHasCompetitionConst {
    public static final String TABLE_NAME = "football_team_has_competition";
    public static final String FOOTBALL_TEAM_ID = "football_team_id";
    public static final String COMPETITION_ID = "competition_id";
    public static final String AMOUNT_OF_WIN = "amount_of_win";

    private FootballTeamHasCompetitionConst() {
    }
}
