package com.igor.model.constant;

public final class ViewConst {
    public static final String SELECT_ALL_TABLE = " - Select all table";
    public static final String SELECT_DB_STRUCTURE = " - Select structure of DB";
    public static final String SELECT_MENU_POINT = "Please, select menu point.";
    public static final String SIGN_QUIT = "Q";
    public static final String SIGN_SELECT_ALL_TABLE = "ALL";
    public static final String SIGN_SELECT_DB_STRUCTURE = "DB";
    public static final String EXIT_MSG = "   Q - exit";
    public static final String MENU_STR = "\nMENU:";
    public static final String TABLE_STR = "\nTable: ";
    public static final String OF_DATABASE_STR = " OF DATABASE: ";
    public static final String PUT_TABLE_IN_MAP = "   %s - Table: %s";
    public static final String TAB = "   ";

    public static final String CITY = "City";
    public static final String COACH = "Coach";
    public static final String COMPETITION = "Competition";
    public static final String FOOTBALLER = "Footballer";
    public static final String FOOTBALL_TEAM_HAS_COMPETITION = "Football Team has Competition";
    public static final String FOOTBALL_TEAM = "Football Team";
    public static final String POSITION = "Position";

    private ViewConst() {
    }
}
