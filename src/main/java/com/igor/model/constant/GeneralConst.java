package com.igor.model.constant;

import com.igor.persistant.ConnectionManager;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;

public final class GeneralConst {
    public static final String FIND_ALL_GENERAL = "SELECT * FROM %s";
    public static final String DELETE_GENERAL = "DELETE FROM %s WHERE %s=?";
    public static final String FIND_BY_ONE_COLUMN_GENERAL = "SELECT * FROM %s WHERE %s=?";

    public static final String DELETED_ROWS_STR = "There are deleted %d rows\n";
    public static final String CREATED_ROWS_STR = "There are created %d rows\n";
    public static final String UPDATED_ROWS_STR = "There are updated %d rows\n";

    public static final int STARTED_POINT = 1;

    public static final BufferedReader INPUT = new BufferedReader(new InputStreamReader(System.in));
    public static final Connection CONNECTION = ConnectionManager.getConnection();

    private GeneralConst() {
    }
}
