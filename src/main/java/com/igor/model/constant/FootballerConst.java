package com.igor.model.constant;

public final class FootballerConst {
    public static final String TABLE_NAME = "footballer";
    public static final String ID = "id";
    public static final String FULL_NAME = "full_name";
    public static final String AGE = "age";
    public static final String MARKET_PRICE = "market_price";
    public static final String SALARY = "salary";
    public static final String FOOTBALL_TEAM_ID = "football_team_id";
    public static final String POSITION_ID = "position_id";

    private FootballerConst() {
    }
}
