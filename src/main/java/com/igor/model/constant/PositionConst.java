package com.igor.model.constant;

public final class PositionConst {
    public static final String TABLE_NAME = "position";
    public static final String ID = "id";
    public static final String NAME = "name";

    public static final int NUMBER_ID_IN_TABLE = 1;
    public static final int NUMBER_NAME_IN_TABLE = 2;

    private PositionConst() {
    }
}
