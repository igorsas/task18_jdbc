package com.igor.model.constant;

public final class CityConst {
    public static final String TABLE_NAME = "city";
    public static final String ID = "id";
    public static final String NAME = "name";

    private CityConst() {
    }
}
