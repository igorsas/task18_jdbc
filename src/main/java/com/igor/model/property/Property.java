package com.igor.model.property;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

import static com.igor.model.constant.PropertyConst.PATH_TO_DB_PROPERTIES;
import static com.igor.view.View.LOG;

public class Property {
    public static String getProperty(final String keyToFile) {
        Properties properties = new Properties();
        try (InputStream input = Property.class.getClassLoader().getResourceAsStream(PATH_TO_DB_PROPERTIES)) {
            properties.load(Objects.requireNonNull(input));
            return properties.getProperty(keyToFile);
        } catch (IOException e) {
            LOG.error(e.getClass());
            LOG.error(e.getMessage());
            LOG.trace(e.getStackTrace());
            return null;
        }
    }
}
