package com.igor.model.metadata;

import static com.igor.model.constant.MetaDataConst.FK_TO_STRING;

public class ForeignKeyMetaData {
    private String pkColumnName;
    private String pkTableName;
    private String fkColumnName;

    public String getPkColunmName() {
        return pkColumnName;
    }

    public void setPkColunmName(final String pkColunmName) {
        this.pkColumnName = pkColunmName;
    }

    public String getPkTableName() {
        return pkTableName;
    }

    public void setPkTableName(final String pkTableName) {
        this.pkTableName = pkTableName;
    }

    public String getFkColumnName() {
        return fkColumnName;
    }

    public void setFkColumnName(final String fkColumnName) {
        this.fkColumnName = fkColumnName;
    }

    @Override
    public String toString() {
        return String.format(FK_TO_STRING, fkColumnName, pkTableName, pkColumnName);
    }
}
