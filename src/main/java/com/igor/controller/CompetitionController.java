package com.igor.controller;

import com.igor.model.entity.CompetitionEntity;
import com.igor.service.implementation.CompetitionServiceImplementation;
import com.igor.service.interfaces.CompetitionService;
import com.igor.view.View;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.igor.controller.Controller.*;
import static com.igor.model.constant.CompetitionConst.*;
import static com.igor.model.constant.GeneralConst.*;
import static com.igor.model.constant.ViewConst.*;
import static com.igor.view.View.showMenu;

public class CompetitionController implements Controller {
    private final Logger logger = View.LOG;
    private final CompetitionService competitionService = new CompetitionServiceImplementation();
    private Map<String, String> menu;
    private Map<String, Controller> methodsMenu;

    @Override
    public void execute() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        writeInstructionsForTable();
        putMethodsInMethodsMenu();
        show();
    }

    private void putMethodsInMethodsMenu() {
        methodsMenu.put("1", this::create);
        methodsMenu.put("2", this::update);
        methodsMenu.put("3", this::deleteFromTable);
        methodsMenu.put("4", this::findCompetitionByID);
        methodsMenu.put("5", this::findCompetitionByName);
        methodsMenu.put("6", this::findCompetitionByPrizeFund);
    }

    private void writeInstructionsForTable() {
        writeGeneralInstructionForTable(menu, COMPETITION);
        int number = 5;
        menu.put(String.valueOf(number),
                String.format("  %s - Find %s by Name", number++, COMPETITION));
        menu.put(String.valueOf(number),
                String.format("  %s - Find %s by Prize Fund", number, COMPETITION));
        menu.put(SIGN_QUIT, EXIT_MSG);
    }

    private void deleteFromTable() throws SQLException {
        int count = competitionService.delete(getIntElementFromTable(ID, TABLE_NAME));
        logger.debug(DELETED_ROWS_STR, count);
    }

    private void create() throws SQLException {
        int count = competitionService.create(createEntity());
        logger.info(CREATED_ROWS_STR, count);
    }

    private void update() throws SQLException {
        int count = competitionService.update(createEntity());
        logger.info(UPDATED_ROWS_STR, count);
    }

    private CompetitionEntity createEntity() {
        return new CompetitionEntity(getIntElementFromTable(ID, TABLE_NAME),
                getLongElementFromTable(PRIZE_FUND, TABLE_NAME),
                getStringElementFromTable(NAME, TABLE_NAME));
    }

    private void findCompetitionByName()
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        competitionService.findByName
                (getStringElementFromTable(NAME, TABLE_NAME))
                .forEach(logger::info);
    }

    private void findCompetitionByPrizeFund()
            throws SQLException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        competitionService.findByPrizeFund
                (getLongElementFromTable(PRIZE_FUND, TABLE_NAME))
                .forEach(logger::info);
    }

    private void findCompetitionByID()
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        CompetitionEntity entity = competitionService.findById(getIntElementFromTable(ID, TABLE_NAME));
        logger.info(entity);
    }

    private void show() {
        showMenu(menu, methodsMenu);
    }
}
