package com.igor.controller;

import com.igor.model.entity.FootballerEntity;
import com.igor.service.implementation.FootballerServiceImplementation;
import com.igor.service.interfaces.FootballerService;
import com.igor.view.View;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.igor.controller.Controller.*;
import static com.igor.model.constant.FootballerConst.*;
import static com.igor.model.constant.GeneralConst.*;
import static com.igor.model.constant.ViewConst.*;
import static com.igor.view.View.showMenu;

public class FootballerController implements Controller {
    private final Logger logger = View.LOG;
    private final FootballerService footballerService = new FootballerServiceImplementation();
    private Map<String, String> menu;
    private Map<String, Controller> methodsMenu;

    @Override
    public void execute() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        writeInstructionsForTable();
        putMethodsInMethodsMenu();
        show();

    }

    private void putMethodsInMethodsMenu() {
        methodsMenu.put("1", this::create);
        methodsMenu.put("2", this::update);
        methodsMenu.put("3", this::deleteFromTable);
        methodsMenu.put("4", this::findFootballerByID);
        methodsMenu.put("5", this::findFootballerByName);
        methodsMenu.put("6", this::findFootballerByAge);
        methodsMenu.put("7", this::findFootballerByMarketPrice);
        methodsMenu.put("8", this::findFootballerBySalary);
        methodsMenu.put("9", this::findFootballerByFootballTeamID);
        methodsMenu.put("10", this::findFootballerByPositionID);
    }

    private void writeInstructionsForTable() {
        writeGeneralInstructionForTable(menu, FOOTBALLER);
        menu.put("5", String.format("  5 - Find %s by Full Name", FOOTBALLER));
        menu.put("6", String.format("  6 - Find %s by Age", FOOTBALLER));
        menu.put("7", String.format("  7 - Find %s by Market Price", FOOTBALLER));
        menu.put("8", String.format("  8 - Find %s by Salary", FOOTBALLER));
        menu.put("9", String.format("  9 - Find %s by Football Team ID", FOOTBALLER));
        menu.put("10", String.format("  10 - Find %s by Position ID", FOOTBALLER));
        menu.put(SIGN_QUIT, EXIT_MSG);
    }

    private void deleteFromTable() throws SQLException {
        int count = footballerService.delete(getIntElementFromTable(ID, TABLE_NAME));
        logger.debug(DELETED_ROWS_STR, count);
    }

    private void create() throws SQLException {
        int count = footballerService.create(createEntity());
        logger.info(CREATED_ROWS_STR, count);
    }


    private void update() throws SQLException {
        int count = footballerService.update(createEntity());
        logger.info(UPDATED_ROWS_STR, count);
    }

    private FootballerEntity createEntity() {
        return new FootballerEntity(getIntElementFromTable(ID, TABLE_NAME),
                getStringElementFromTable(FULL_NAME, TABLE_NAME),
                getIntElementFromTable(AGE, TABLE_NAME),
                getLongElementFromTable(MARKET_PRICE, TABLE_NAME),
                getLongElementFromTable(SALARY, TABLE_NAME),
                getIntElementFromTable(FOOTBALL_TEAM_ID, TABLE_NAME),
                getIntElementFromTable(POSITION_ID, TABLE_NAME));
    }

    private void findFootballerByName()
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        footballerService.findByFullName
                (getStringElementFromTable(FULL_NAME, TABLE_NAME))
                .forEach(logger::info);
    }

    private void findFootballerByID()
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        FootballerEntity coach = footballerService.findById(getIntElementFromTable(ID, TABLE_NAME));
        logger.info(coach);
    }

    private void findFootballerByFootballTeamID()
            throws SQLException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        footballerService.findByFootballTeamId
                (getIntElementFromTable(FOOTBALL_TEAM_ID, TABLE_NAME))
                .forEach(logger::info);
    }

    private void findFootballerBySalary()
            throws SQLException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        footballerService.findBySalary
                (getLongElementFromTable(SALARY, TABLE_NAME))
                .forEach(logger::info);
    }

    private void findFootballerByPositionID()
            throws InvocationTargetException, SQLException, InstantiationException, IllegalAccessException, NoSuchMethodException {
        footballerService.findByPositionId
                (getIntElementFromTable(POSITION_ID, TABLE_NAME))
                .forEach(logger::info);
    }

    private void findFootballerByMarketPrice()
            throws InvocationTargetException, SQLException, InstantiationException, IllegalAccessException, NoSuchMethodException {
        footballerService.findByMarketPrice
                (getLongElementFromTable(MARKET_PRICE, TABLE_NAME))
                .forEach(logger::info);
    }

    private void findFootballerByAge()
            throws InvocationTargetException, SQLException, InstantiationException, IllegalAccessException, NoSuchMethodException {
        footballerService.findByAge
                (getIntElementFromTable(AGE, TABLE_NAME))
                .forEach(logger::info);
    }

    private void show() {
        showMenu(menu, methodsMenu);
    }
}
