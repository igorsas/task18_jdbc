package com.igor.controller;

import com.igor.model.entity.PositionEntity;
import com.igor.service.implementation.PositionServiceImplementation;
import com.igor.service.interfaces.PositionService;
import com.igor.view.View;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.igor.controller.Controller.getIntElementFromTable;
import static com.igor.controller.Controller.getStringElementFromTable;
import static com.igor.model.constant.GeneralConst.*;
import static com.igor.model.constant.PositionConst.*;
import static com.igor.model.constant.ViewConst.*;
import static com.igor.view.View.showMenu;

public class PositionController implements Controller {
    private final Logger logger = View.LOG;
    private final PositionService cityService = new PositionServiceImplementation();
    private Map<String, String> menu;
    private Map<String, Controller> methodsMenu;

    @Override
    public void execute() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        writeInstructionsForTable();
        putMethodsInMethodsMenu();
        show();

    }

    private void putMethodsInMethodsMenu() {
        methodsMenu.put("1", this::create);
        methodsMenu.put("2", this::update);
        methodsMenu.put("3", this::deleteFromTable);
        methodsMenu.put("4", this::findPositionByID);
        methodsMenu.put("5", this::findPositionByName);
    }

    private void writeInstructionsForTable() {
        Controller.writeGeneralInstructionForTable(menu, POSITION);
        menu.put(String.valueOf(5),
                String.format("  %s - Find %s by Name", 5, POSITION));
        menu.put(SIGN_QUIT, EXIT_MSG);
    }

    private void deleteFromTable() throws SQLException {
        int count = cityService.delete(getIntElementFromTable(ID, TABLE_NAME));
        logger.debug(DELETED_ROWS_STR, count);
    }

    private void create() throws SQLException {
        int count = cityService.create(createEntity());
        logger.info(CREATED_ROWS_STR, count);
    }

    private void update() throws SQLException {
        int count = cityService.update(createEntity());
        logger.info(UPDATED_ROWS_STR, count);
    }

    private PositionEntity createEntity() {
        return new PositionEntity(getIntElementFromTable(ID, TABLE_NAME),
                getStringElementFromTable(NAME, TABLE_NAME));

    }

    private void findPositionByName()
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        cityService.findByName(getStringElementFromTable(NAME, TABLE_NAME))
                .forEach(logger::info);

    }

    private void findPositionByID()
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        PositionEntity entity = cityService.findById(getIntElementFromTable(ID, TABLE_NAME));
        logger.info(entity);
    }

    private void show() {
        showMenu(menu, methodsMenu);
    }
}
