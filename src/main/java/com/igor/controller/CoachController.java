package com.igor.controller;

import com.igor.model.entity.CoachEntity;
import com.igor.service.implementation.CoachServiceImplementation;
import com.igor.service.interfaces.CoachService;
import com.igor.view.View;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.igor.controller.Controller.*;
import static com.igor.model.constant.CoachConst.*;
import static com.igor.model.constant.GeneralConst.*;
import static com.igor.model.constant.ViewConst.*;
import static com.igor.view.View.showMenu;

public class CoachController implements Controller {
    private final Logger logger = View.LOG;
    private final CoachService coachService = new CoachServiceImplementation();
    private Map<String, String> menu;
    private Map<String, Controller> methodsMenu;

    @Override
    public void execute() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        writeInstructionsForTable();
        putMethodsInMethodsMenu();
        show();
    }

    private void putMethodsInMethodsMenu() {
        methodsMenu.put("1", this::create);
        methodsMenu.put("2", this::update);
        methodsMenu.put("3", this::deleteFromTable);
        methodsMenu.put("4", this::findCoachByID);
        methodsMenu.put("5", this::findCoachByName);
        methodsMenu.put("6", this::findCoachBySalary);
        methodsMenu.put("7", this::findCoachByFootballTeamID);
    }

    private void writeInstructionsForTable() {
        writeGeneralInstructionForTable(menu, COACH);
        int number = 5;
        menu.put(String.valueOf(number),
                String.format("  %s - Find %s by Name", number++, COACH));
        menu.put(String.valueOf(number),
                String.format("  %s - Find %s by Salary", number++, COACH));
        menu.put(String.valueOf(number),
                String.format("  %s - Find %s by Football Team ID", number, COACH));

        menu.put(SIGN_QUIT, EXIT_MSG);
    }

    private void deleteFromTable() throws SQLException {
        int count = coachService.delete(getIntElementFromTable(ID, TABLE_NAME));
        logger.debug(DELETED_ROWS_STR, count);
    }

    private void create() throws SQLException {
        int count = coachService.create(createEntity());
        logger.info(CREATED_ROWS_STR, count);
    }

    private void update() throws SQLException {
        int count = coachService.update(createEntity());
        logger.info(UPDATED_ROWS_STR, count);
    }

    private CoachEntity createEntity() {
        return new CoachEntity(getIntElementFromTable(ID, TABLE_NAME),
                getStringElementFromTable(NAME, TABLE_NAME),
                getLongElementFromTable(SALARY, TABLE_NAME),
                getIntElementFromTable(FOOTBALL_TEAM_ID, TABLE_NAME));
    }

    private void findCoachByName()
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        coachService.findByName(getStringElementFromTable(NAME, TABLE_NAME)).forEach(logger::info);
    }

    private void findCoachByID()
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        CoachEntity coach = coachService.findById(getIntElementFromTable(ID, TABLE_NAME));
        logger.info(coach);
    }

    private void findCoachByFootballTeamID()
            throws SQLException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        CoachEntity coach = coachService.findByFootballTeamId(getIntElementFromTable(FOOTBALL_TEAM_ID, TABLE_NAME));
        logger.info(coach);
    }

    private void findCoachBySalary()
            throws SQLException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        coachService.findBySalary(getIntElementFromTable(FOOTBALL_TEAM_ID, TABLE_NAME))
                .forEach(logger::info);
    }

    private void show() {
        showMenu(menu, methodsMenu);
    }
}
