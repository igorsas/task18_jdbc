package com.igor.controller;

import com.igor.model.entity.FootballTeamHasCompetitionEntity;
import com.igor.model.entity.PrimaryKeyFootballCompetition;
import com.igor.service.implementation.FootballTeamHasCompetitionServiceImplementation;
import com.igor.service.interfaces.FootballTeamHasCompetitionService;
import com.igor.view.View;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.igor.controller.Controller.getIntElementFromTable;
import static com.igor.model.constant.FootballTeamHasCompetitionConst.*;
import static com.igor.model.constant.GeneralConst.*;
import static com.igor.model.constant.ViewConst.*;
import static com.igor.view.View.showMenu;

public class FootballTeamHasCompetitionController implements Controller {
    private final Logger logger = View.LOG;
    private final FootballTeamHasCompetitionService competitionService = new FootballTeamHasCompetitionServiceImplementation();
    private Map<String, String> menu;
    private Map<String, Controller> methodsMenu;

    @Override
    public void execute() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        writeInstructionsForTable();
        putMethodsInMethodsMenu();
        show();

    }

    private void putMethodsInMethodsMenu() {
        methodsMenu.put("1", this::create);
        methodsMenu.put("2", this::update);
        methodsMenu.put("3", this::deleteFromTable);
        methodsMenu.put("4", this::findFootballTeamHasCompetitionByID);
        methodsMenu.put("5", this::findFootballTeamHasCompetitionByTeamId);
        methodsMenu.put("6", this::findFootballTeamHasCompetitionByCompetitionId);
        methodsMenu.put("7", this::findFootballTeamHasCompetitionByAmountOfWin);
    }

    private void writeInstructionsForTable() {
        Controller.writeGeneralInstructionForTable(menu, FOOTBALL_TEAM_HAS_COMPETITION);
        int number = 5;
        menu.put(String.valueOf(number),
                String.format("  %s - Find %s by football team ID", number++, FOOTBALL_TEAM_HAS_COMPETITION));
        menu.put(String.valueOf(number),
                String.format("  %s - Find %s by competition ID", number++, FOOTBALL_TEAM_HAS_COMPETITION));
        menu.put(String.valueOf(number),
                String.format("  %s - Find %s by Amount of Win", number, FOOTBALL_TEAM_HAS_COMPETITION));
        menu.put(SIGN_QUIT, EXIT_MSG);
    }

    private void deleteFromTable() throws SQLException {
        int count = competitionService.delete(new PrimaryKeyFootballCompetition
                (getIntElementFromTable(FOOTBALL_TEAM_ID, TABLE_NAME),
                        getIntElementFromTable(COMPETITION_ID, TABLE_NAME)));
        logger.debug(DELETED_ROWS_STR, count);
    }

    private void create() throws SQLException {
        int count = competitionService.create(createEntity());
        logger.info(CREATED_ROWS_STR, count);
    }

    private void update() throws SQLException {
        int count = competitionService.update(createEntity());
        logger.info(UPDATED_ROWS_STR, count);
    }

    private FootballTeamHasCompetitionEntity createEntity() {
        return new FootballTeamHasCompetitionEntity(new PrimaryKeyFootballCompetition(
                getIntElementFromTable(FOOTBALL_TEAM_ID, TABLE_NAME),
                getIntElementFromTable(COMPETITION_ID, TABLE_NAME)),
                getIntElementFromTable(AMOUNT_OF_WIN, TABLE_NAME));
    }

    private void findFootballTeamHasCompetitionByTeamId()
            throws SQLException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        competitionService.findByFootballTeamId
                (getIntElementFromTable(FOOTBALL_TEAM_ID, TABLE_NAME))
                .forEach(logger::info);
    }

    private void findFootballTeamHasCompetitionByCompetitionId()
            throws SQLException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        competitionService.findByCompetitionId
                (getIntElementFromTable(COMPETITION_ID, TABLE_NAME))
                .forEach(logger::info);

    }

    private void findFootballTeamHasCompetitionByAmountOfWin()
            throws SQLException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        List<FootballTeamHasCompetitionEntity> cities = competitionService.findByAmountOfWin
                (getIntElementFromTable(AMOUNT_OF_WIN, TABLE_NAME));
        for (FootballTeamHasCompetitionEntity entity : cities) {
            logger.info(entity);
        }
    }

    private void findFootballTeamHasCompetitionByID()
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        FootballTeamHasCompetitionEntity entity = competitionService.findById(
                new PrimaryKeyFootballCompetition(getIntElementFromTable(FOOTBALL_TEAM_ID, TABLE_NAME),
                        getIntElementFromTable(COMPETITION_ID, TABLE_NAME)));
        logger.info(entity);
    }

    private void show() {
        showMenu(menu, methodsMenu);
    }
}
