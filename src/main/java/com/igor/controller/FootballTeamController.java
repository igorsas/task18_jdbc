package com.igor.controller;

import com.igor.model.entity.FootballTeamEntity;
import com.igor.service.implementation.FootballTeamServiceImplementation;
import com.igor.service.interfaces.FootballTeamService;
import com.igor.view.View;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.igor.controller.Controller.getIntElementFromTable;
import static com.igor.controller.Controller.getStringElementFromTable;
import static com.igor.model.constant.FootballTeamConst.*;
import static com.igor.model.constant.GeneralConst.*;
import static com.igor.model.constant.ViewConst.*;
import static com.igor.view.View.showMenu;

public class FootballTeamController implements Controller {
    private final Logger logger = View.LOG;
    private final FootballTeamService footballTeamService = new FootballTeamServiceImplementation();
    private Map<String, String> menu;
    private Map<String, Controller> methodsMenu;

    @Override
    public void execute() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        writeInstructionsForTable();
        putMethodsInMethodsMenu();
        show();
    }

    private void putMethodsInMethodsMenu() {
        methodsMenu.put("1", this::create);
        methodsMenu.put("2", this::update);
        methodsMenu.put("3", this::deleteFromTable);
        methodsMenu.put("4", this::findFootballTeamByID);
        methodsMenu.put("5", this::findCityByName);
        methodsMenu.put("6", this::findFootballerByAge);
        methodsMenu.put("7", this::findFootballerByCityID);
    }

    private void writeInstructionsForTable() {
        Controller.writeGeneralInstructionForTable(menu, FOOTBALL_TEAM);
        menu.put("5", String.format("  5 - Find %s by Full Name", FOOTBALL_TEAM));
        menu.put("6", String.format("  6 - Find %s by Age", FOOTBALL_TEAM));
        menu.put("7", String.format("  7 - Find %s by City ID", FOOTBALL_TEAM));
        menu.put(SIGN_QUIT, EXIT_MSG);
    }

    private void deleteFromTable() throws SQLException {
        int count = footballTeamService.delete(getIntElementFromTable(ID, TABLE_NAME));
        logger.debug(DELETED_ROWS_STR, count);
    }

    private void create() throws SQLException {
        int count = footballTeamService.create(createEntity());
        logger.info(CREATED_ROWS_STR, count);
    }


    private void update() throws SQLException {
        int count = footballTeamService.update(createEntity());
        logger.info(UPDATED_ROWS_STR, count);
    }

    private FootballTeamEntity createEntity() {
        return new FootballTeamEntity(getIntElementFromTable(ID, TABLE_NAME),
                getStringElementFromTable(NAME, TABLE_NAME),
                getIntElementFromTable(AGE, TABLE_NAME),
                getIntElementFromTable(CITY_ID, TABLE_NAME));
    }

    private void findCityByName()
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        footballTeamService.findByName
                (getStringElementFromTable(NAME, TABLE_NAME))
                .forEach(logger::info);
    }

    private void findFootballTeamByID()
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        FootballTeamEntity footballTeamEntity = footballTeamService.findById
                (getIntElementFromTable(ID, TABLE_NAME));
        logger.info(footballTeamEntity);
    }

    private void findFootballerByCityID()
            throws InvocationTargetException, SQLException, InstantiationException, IllegalAccessException, NoSuchMethodException {
        footballTeamService.findByCityId
                (getIntElementFromTable(CITY_ID, TABLE_NAME))
                .forEach(logger::info);
    }

    private void findFootballerByAge() throws InvocationTargetException, SQLException, InstantiationException, IllegalAccessException, NoSuchMethodException {
        footballTeamService.findByAge
                (getIntElementFromTable(AGE, TABLE_NAME))
                .forEach(logger::info);
    }

    private void show() {
        showMenu(menu, methodsMenu);
    }
}
