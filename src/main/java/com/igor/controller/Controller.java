package com.igor.controller;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.Map;

import static com.igor.model.constant.GeneralConst.INPUT;
import static com.igor.model.constant.GeneralConst.STARTED_POINT;
import static com.igor.view.View.LOG;

public interface Controller {
    static void writeGeneralInstructionForTable(Map<String, String> menu, String table) {
        int number = STARTED_POINT;
        menu.put(String.valueOf(number),
                String.format("  %s - Create for %s", number++, table));
        menu.put(String.valueOf(number),
                String.format("  %s - Update %s", number++, table));
        menu.put(String.valueOf(number),
                String.format("  %s - Delete from %s", number++, table));
        menu.put(String.valueOf(number),
                String.format("  %s - Find %s by ID", number, table));
    }

    static int getIntElementFromTable(String element, String table) {
        LOG.info(String.format("Input %s for %s: ", element, table));
        try {
            return Integer.parseInt(INPUT.readLine());
        } catch (IOException | NumberFormatException e) {
            LOG.error(e.getClass());
            LOG.error(e.getMessage());
            LOG.trace(e.getStackTrace());
        }
        return 0;
    }

    static long getLongElementFromTable(String element, String table) {
        LOG.info(String.format("Input %s for %s: ", element, table));
        try {
            return Long.parseLong(INPUT.readLine());
        } catch (IOException | NumberFormatException e) {
            LOG.error(e.getClass());
            LOG.error(e.getMessage());
            LOG.trace(e.getStackTrace());
        }
        return 0;
    }

    static String getStringElementFromTable(String element, String table) {
        LOG.info(String.format("Input %s for %s: ", element, table));
        try {
            return INPUT.readLine();
        } catch (IOException | NumberFormatException e) {
            LOG.error(e.getClass());
            LOG.error(e.getMessage());
            LOG.trace(e.getStackTrace());
        }
        return element;
    }

    void execute() throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;
}
