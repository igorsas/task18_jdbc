package com.igor.transformer;


import com.igor.model.annotation.Column;
import com.igor.model.annotation.PrimaryKeyComposite;
import com.igor.model.annotation.Table;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Transformer<T> {
    private final Class<T> clazz;

    public Transformer(final Class<T> clazz) {
        this.clazz = clazz;
    }

    public Object fromResultSetToEntity(final ResultSet rs)
            throws SQLException, InvocationTargetException, NoSuchMethodException, IllegalAccessException, InstantiationException {
        Object entity;
        entity = clazz.getConstructor().newInstance();
        if (clazz.isAnnotationPresent(Table.class)) {
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                if (field.isAnnotationPresent(Column.class)) {
                    setInformationInField(rs, entity, field);
                } else {
                    fromResultSetToEntityWithCompositePK(rs, entity, field);
                }
            }
        }
        return entity;
    }

    private void fromResultSetToEntityWithCompositePK(final ResultSet rs, final Object entity, final Field field)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, SQLException {
        if (field.isAnnotationPresent(PrimaryKeyComposite.class)) {
            field.setAccessible(true);
            Class fieldType = field.getType();
            Object foreignKey = fieldType.getConstructor().newInstance();
            field.set(entity, foreignKey);
            Field[] fieldsInner = fieldType.getDeclaredFields();
            for (Field fieldInner : fieldsInner) {
                if (fieldInner.isAnnotationPresent(Column.class)) {
                    setInformationInField(rs, foreignKey, fieldInner);
                }
            }
        }
    }

    private void setInformationInField(final ResultSet rs, final Object fk, final Field fieldInner)
            throws IllegalAccessException, SQLException {
        Column column = fieldInner.getAnnotation(Column.class);
        String name = column.name();
        fieldInner.setAccessible(true);
        Class fieldInnerType = fieldInner.getType();
        if (fieldInnerType == String.class) {
            fieldInner.set(fk, rs.getString(name));
        } else if (fieldInnerType == int.class) {
            fieldInner.set(fk, rs.getInt(name));
        } else if (fieldInnerType == long.class) {
            fieldInner.set(fk, rs.getLong(name));
        }
    }
}
