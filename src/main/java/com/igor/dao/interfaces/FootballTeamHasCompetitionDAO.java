package com.igor.dao.interfaces;

import com.igor.model.entity.FootballTeamHasCompetitionEntity;
import com.igor.model.entity.PrimaryKeyFootballCompetition;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public interface FootballTeamHasCompetitionDAO extends GeneralDAO<FootballTeamHasCompetitionEntity, PrimaryKeyFootballCompetition> {
    List<FootballTeamHasCompetitionEntity> findByCompetitionId(int competitionId) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;

    List<FootballTeamHasCompetitionEntity> findByFootballTeamId(int competitionId) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;

    List<FootballTeamHasCompetitionEntity> findByAmountOfWin(int amountOfWin) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;
}
