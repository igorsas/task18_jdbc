package com.igor.dao.interfaces;

import com.igor.model.entity.FootballTeamEntity;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public interface FootballTeamDAO extends GeneralDAO<FootballTeamEntity, Integer> {
    List<FootballTeamEntity> findByName(String name) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;

    List<FootballTeamEntity> findByAge(int age) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;

    List<FootballTeamEntity> findByCityId(int cityId) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;
}
