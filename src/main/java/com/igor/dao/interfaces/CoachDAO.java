package com.igor.dao.interfaces;

import com.igor.model.entity.CoachEntity;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public interface CoachDAO extends GeneralDAO<CoachEntity, Integer> {
    List<CoachEntity> findByName(String name) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;

    List<CoachEntity> findBySalary(long salary) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;

    CoachEntity findByFootballTeamId(int footballTeamId) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;
}
