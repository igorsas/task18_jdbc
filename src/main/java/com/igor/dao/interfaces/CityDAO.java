package com.igor.dao.interfaces;

import com.igor.model.entity.CityEntity;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public interface CityDAO extends GeneralDAO<CityEntity, Integer> {
    List<CityEntity> findByName(String name) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;
}
