package com.igor.dao.interfaces;

import com.igor.model.entity.PositionEntity;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public interface PositionDAO extends GeneralDAO<PositionEntity, Integer> {
    List<PositionEntity> findByName(String name) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;
}
