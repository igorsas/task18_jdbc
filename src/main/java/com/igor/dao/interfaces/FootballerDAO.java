package com.igor.dao.interfaces;

import com.igor.model.entity.FootballerEntity;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public interface FootballerDAO extends GeneralDAO<FootballerEntity, Integer> {
    List<FootballerEntity> findByFullName(String fullName) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;

    List<FootballerEntity> findByAge(int age) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;

    List<FootballerEntity> findByMarketPrice(long marketPrice) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;

    List<FootballerEntity> findBySalary(long salary) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;

    List<FootballerEntity> findByFootballTeamId(int footballTeamId) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;

    List<FootballerEntity> findByPositionId(int positionId) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;
}
