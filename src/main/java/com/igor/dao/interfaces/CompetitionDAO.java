package com.igor.dao.interfaces;

import com.igor.model.entity.CompetitionEntity;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public interface CompetitionDAO extends GeneralDAO<CompetitionEntity, Integer> {
    List<CompetitionEntity> findByName(String name) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;

    List<CompetitionEntity> findByPrizeFund(long prizeFund) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;

}
