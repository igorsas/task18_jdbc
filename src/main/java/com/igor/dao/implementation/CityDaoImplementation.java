package com.igor.dao.implementation;

import com.igor.dao.interfaces.CityDAO;
import com.igor.model.entity.CityEntity;
import com.igor.transformer.Transformer;

import java.lang.reflect.InvocationTargetException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static com.igor.model.constant.CityConst.*;
import static com.igor.model.constant.GeneralConst.*;

public class CityDaoImplementation implements CityDAO {
    private static final String FIND_ALL = String.format(FIND_ALL_GENERAL, TABLE_NAME);
    private static final String DELETE = String.format(DELETE_GENERAL, TABLE_NAME, ID);
    private static final String CREATE = String.format("INSERT %s (%s, %s) VALUES (?, ?)", TABLE_NAME, ID, NAME);
    private static final String UPDATE = String.format("UPDATE %s SET %s=? WHERE %s=?", TABLE_NAME, NAME, ID);
    private static final String FIND_BY_ID = String.format(FIND_BY_ONE_COLUMN_GENERAL, TABLE_NAME, ID);
    private static final String FIND_BY_NAME = String.format(FIND_BY_ONE_COLUMN_GENERAL, TABLE_NAME, NAME);

    @Override
    public List<CityEntity> findByName(final String name) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        List<CityEntity> cities = new ArrayList<>();
        try (PreparedStatement ps = CONNECTION.prepareStatement(FIND_BY_NAME)) {
            ps.setString(1, name);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    cities.add((CityEntity) new Transformer<>(CityEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return cities;
    }

    @Override
    public List<CityEntity> findAll() throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        List<CityEntity> cities = new ArrayList<>();
        try (Statement statement = CONNECTION.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    cities.add((CityEntity) new Transformer<>(CityEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return cities;
    }

    @Override
    public CityEntity findById(final Integer id) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        CityEntity entity = null;
        try (PreparedStatement ps = CONNECTION.prepareStatement(FIND_BY_ID)) {
            ps.setInt(1, id);
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    entity = (CityEntity) new Transformer<>(CityEntity.class).fromResultSetToEntity(resultSet);
                }
            }
        }
        return entity;
    }

    @Override
    public int create(final CityEntity entity) throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(CREATE)) {
            ps.setString(1, String.valueOf(entity.getId()));
            ps.setString(2, entity.getName());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(final CityEntity entity) throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(UPDATE)) {
            ps.setString(1, entity.getName());
            ps.setString(2, String.valueOf(entity.getId()));
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(final Integer id) throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(DELETE)) {
            ps.setInt(1, id);
            return ps.executeUpdate();
        }
    }
}
