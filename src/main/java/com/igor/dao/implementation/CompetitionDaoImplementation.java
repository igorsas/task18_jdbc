package com.igor.dao.implementation;

import com.igor.dao.interfaces.CompetitionDAO;
import com.igor.model.entity.CompetitionEntity;
import com.igor.transformer.Transformer;

import java.lang.reflect.InvocationTargetException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static com.igor.model.constant.CompetitionConst.*;
import static com.igor.model.constant.GeneralConst.*;


public class CompetitionDaoImplementation implements CompetitionDAO {
    private static final String FIND_ALL = String.format(FIND_ALL_GENERAL, TABLE_NAME);
    private static final String DELETE = String.format(DELETE_GENERAL, TABLE_NAME, ID);
    private static final String CREATE = String.format("INSERT %s (%s, %s, %s) VALUES (?, ?, ?)",
            TABLE_NAME, ID, PRIZE_FUND, NAME);
    private static final String UPDATE = String.format("UPDATE %s SET %s=?, %s=? WHERE %s=?",
            TABLE_NAME, PRIZE_FUND, NAME, ID);
    private static final String FIND_BY_ID = String.format(FIND_BY_ONE_COLUMN_GENERAL, TABLE_NAME, ID);
    private static final String FIND_BY_NAME = String.format(FIND_BY_ONE_COLUMN_GENERAL, TABLE_NAME, NAME);
    private static final String FIND_BY_PRIZE_FUND = String.format(FIND_BY_ONE_COLUMN_GENERAL, TABLE_NAME, PRIZE_FUND);

    @Override
    public List<CompetitionEntity> findByName(final String name) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        List<CompetitionEntity> competitions = new ArrayList<>();
        try (PreparedStatement ps = CONNECTION.prepareStatement(FIND_BY_NAME)) {
            ps.setString(1, name);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    competitions.add((CompetitionEntity) new Transformer<>(CompetitionEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return competitions;
    }

    @Override
    public List<CompetitionEntity> findByPrizeFund(final long prizeFund) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        List<CompetitionEntity> competitions = new ArrayList<>();
        try (PreparedStatement ps = CONNECTION.prepareStatement(FIND_BY_PRIZE_FUND)) {
            ps.setString(1, String.valueOf(prizeFund));
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    competitions.add((CompetitionEntity) new Transformer<>(CompetitionEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return competitions;
    }

    @Override
    public List<CompetitionEntity> findAll() throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        List<CompetitionEntity> competitions = new ArrayList<>();
        try (Statement statement = CONNECTION.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    competitions.add((CompetitionEntity) new Transformer<>(CompetitionEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return competitions;
    }


    @Override
    public CompetitionEntity findById(final Integer id) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        CompetitionEntity entity = null;
        try (PreparedStatement ps = CONNECTION.prepareStatement(FIND_BY_ID)) {
            ps.setString(1, String.valueOf(id));
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    entity = (CompetitionEntity) new Transformer<>(CompetitionEntity.class).fromResultSetToEntity(resultSet);
                }
            }
        }
        return entity;
    }

    @Override
    public int create(final CompetitionEntity entity) throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(CREATE)) {
            ps.setString(1, String.valueOf(entity.getId()));
            ps.setString(2, String.valueOf(entity.getPrizeFund()));
            ps.setString(3, entity.getName());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(final CompetitionEntity entity) throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(UPDATE)) {
            ps.setString(1, String.valueOf(entity.getPrizeFund()));
            ps.setString(2, entity.getName());
            ps.setString(3, String.valueOf(entity.getId()));
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(final Integer id) throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(DELETE)) {
            ps.setString(1, String.valueOf(id));
            return ps.executeUpdate();
        }
    }
}
