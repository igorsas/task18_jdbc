package com.igor.dao.implementation;


import com.igor.model.metadata.ColumnMetaData;
import com.igor.model.metadata.ForeignKeyMetaData;
import com.igor.model.metadata.TableMetaData;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.igor.model.constant.GeneralConst.CONNECTION;
import static com.igor.model.constant.MetaDataConst.*;

public class MetaDataDaoImpl {

    public List<String> findAllTableName() throws SQLException {
        List<String> tableNames = new ArrayList<>();
        String[] types = {TABLE};
        DatabaseMetaData databaseMetaData = CONNECTION.getMetaData();
        ResultSet result = databaseMetaData.getTables(CONNECTION.getCatalog(), null, COLUMN_NAME_PATTERN, types);

        while (result.next()) {
            String tableName = result.getString(TABLE_NAME);
            tableNames.add(tableName);
        }
        return tableNames;
    }

    public List<TableMetaData> getTablesStructure() throws SQLException {
        List<TableMetaData> tableMetaDataList = new ArrayList<>();
        DatabaseMetaData databaseMetaData = CONNECTION.getMetaData();
        String[] types = {TABLE};
        String dbName = CONNECTION.getCatalog();
        ResultSet result = databaseMetaData.getTables(dbName, null, COLUMN_NAME_PATTERN, types);
        while (result.next()) {
            String tableName = result.getString(TABLE_NAME);
            TableMetaData tableMetaData = getTableMetaData(dbName, tableName);
            setPKMetaData(dbName, tableMetaData, databaseMetaData, tableName);
            setFKMetaData(dbName, tableMetaData, databaseMetaData, tableName);
            tableMetaDataList.add(tableMetaData);
        }
        return tableMetaDataList;
    }

    private void setFKMetaData(final String dbName, final TableMetaData tableMetaData,
                               final DatabaseMetaData databaseMetaData, final String tableName)
            throws SQLException {
        List<ForeignKeyMetaData> fkMetaDataList = new ArrayList<>();
        ResultSet foreignKeysResultRes = databaseMetaData.getImportedKeys(dbName, null, tableName);
        setFKColumnMetaData(foreignKeysResultRes, fkMetaDataList);
        tableMetaData.setForeignKeyList(fkMetaDataList);
    }

    private TableMetaData getTableMetaData(final String dbName, final String tableName) {
        TableMetaData tableMetaData = new TableMetaData();
        tableMetaData.setDBName(dbName);
        tableMetaData.setTableName(tableName);
        return tableMetaData;
    }

    private void setPKMetaData(final String dbName, final TableMetaData tableMetaData,
                               final DatabaseMetaData databaseMetaData, final String tableName)
            throws SQLException {
        List<String> pkList = new ArrayList<>();
        ResultSet primaryKeys = databaseMetaData.getPrimaryKeys(CONNECTION.getCatalog(), null, tableName);
        while (primaryKeys.next()) {
            pkList.add(primaryKeys.getString(COLUMN_NAME));
        }
        List<ColumnMetaData> columnsMetaData = new ArrayList<>();
        ResultSet columnsRS = databaseMetaData.getColumns(dbName, null, tableName, COLUMN_NAME_PATTERN);
        setColumnMetaData(columnsRS, columnsMetaData, pkList);
        tableMetaData.setColumnMetaData(columnsMetaData);
    }

    private void setColumnMetaData(final ResultSet columnsRS,
                                   final List<ColumnMetaData> columnsMetaData,
                                   final List<String> pkList)
            throws SQLException {
        while (columnsRS.next()) {
            ColumnMetaData columnMetaData = new ColumnMetaData();
            columnMetaData.setColumnName(columnsRS.getString(COLUMN_NAME));
            columnMetaData.setDataType(columnsRS.getString(TYPE_NAME));
            columnMetaData.setColumnSize(columnsRS.getString(COLUMN_SIZE));
            columnMetaData.setDecimalDigits(columnsRS.getString(DECIMAL_DIGITS));
            columnMetaData.setNullable(columnsRS.getString(IS_NULLABLE).equals(YES));
            columnMetaData.setAutoIncrement(columnsRS.getString(IS_AUTOINCREMENT).equals(IS_AUTOINCREMENT));
            columnMetaData.setPrimaryKey(false);
            for (String pkName : pkList) {
                if (columnMetaData.getColumnName().equals(pkName)) {
                    columnMetaData.setPrimaryKey(true);
                    break;
                }
            }
            columnsMetaData.add(columnMetaData);
        }
    }

    private void setFKColumnMetaData(final ResultSet foreignKeysResultRes,
                                     final List<ForeignKeyMetaData> fkMetaDataList)
            throws SQLException {
        while (foreignKeysResultRes.next()) {
            ForeignKeyMetaData fk = new ForeignKeyMetaData();
            fk.setFkColumnName(foreignKeysResultRes.getString(FK_COLUMN_NAME));
            fk.setPkTableName(foreignKeysResultRes.getString(PK_TABLE_NAME));
            fk.setPkColunmName(foreignKeysResultRes.getString(PK_COLUMN_NAME));
            fkMetaDataList.add(fk);
        }
    }
}
