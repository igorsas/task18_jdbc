package com.igor.dao.implementation;

import com.igor.dao.interfaces.FootballTeamDAO;
import com.igor.model.entity.FootballTeamEntity;
import com.igor.transformer.Transformer;

import java.lang.reflect.InvocationTargetException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static com.igor.model.constant.FootballTeamConst.*;
import static com.igor.model.constant.GeneralConst.*;

public class FootballTeamDaoImplementation implements FootballTeamDAO {
    private static final String FIND_ALL = String.format(FIND_ALL_GENERAL, TABLE_NAME);
    private static final String DELETE = String.format(DELETE_GENERAL, TABLE_NAME, ID);
    private static final String CREATE = String.format("INSERT %s (%s, %s, %s, %s) VALUES (?, ?, ?, ?)",
            TABLE_NAME, ID, NAME, AGE, CITY_ID);
    private static final String UPDATE = String.format("UPDATE %s SET %s=?, %s=?, %s=? WHERE %s=?",
            TABLE_NAME, NAME, AGE, CITY_ID, ID);
    private static final String FIND_BY_ID = String.format(FIND_BY_ONE_COLUMN_GENERAL, TABLE_NAME, ID);
    private static final String FIND_BY_NAME = String.format(FIND_BY_ONE_COLUMN_GENERAL, TABLE_NAME, NAME);
    private static final String FIND_BY_AGE = String.format(FIND_BY_ONE_COLUMN_GENERAL, TABLE_NAME, AGE);
    private static final String FIND_BY_CITY_ID = String.format(FIND_BY_ONE_COLUMN_GENERAL, TABLE_NAME, CITY_ID);

    @Override
    public List<FootballTeamEntity> findByName(final String name) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        List<FootballTeamEntity> footballTeams = new ArrayList<>();
        try (PreparedStatement ps = CONNECTION.prepareStatement(FIND_BY_NAME)) {
            ps.setString(1, name);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    footballTeams.add((FootballTeamEntity) new Transformer<>(FootballTeamEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return footballTeams;
    }

    @Override
    public List<FootballTeamEntity> findByAge(final int age) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return findByNumberElement(age, FIND_BY_AGE);
    }

    @Override
    public List<FootballTeamEntity> findByCityId(final int id) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return findByNumberElement(id, FIND_BY_CITY_ID);
    }

    private List<FootballTeamEntity> findByNumberElement(final Number element, final String findByNameInTable)
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        List<FootballTeamEntity> footballTeams = new ArrayList<>();
        try (PreparedStatement ps = CONNECTION.prepareStatement(findByNameInTable)) {
            ps.setString(1, String.valueOf(element));
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    footballTeams.add((FootballTeamEntity) new Transformer<>(FootballTeamEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return footballTeams;
    }

    @Override
    public List<FootballTeamEntity> findAll()
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        List<FootballTeamEntity> footballTeams = new ArrayList<>();
        try (Statement statement = CONNECTION.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    footballTeams.add((FootballTeamEntity) new Transformer<>(FootballTeamEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return footballTeams;
    }


    @Override
    public FootballTeamEntity findById(final Integer id)
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        FootballTeamEntity entity = null;
        try (PreparedStatement ps = CONNECTION.prepareStatement(FIND_BY_ID)) {
            ps.setString(1, String.valueOf(id));
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity = (FootballTeamEntity) new Transformer<>(FootballTeamEntity.class).fromResultSetToEntity(resultSet);
                    break;
                }
            }
        }
        return entity;
    }

    @Override
    public int create(final FootballTeamEntity entity) throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(CREATE)) {
            ps.setString(1, String.valueOf(entity.getId()));
            ps.setString(2, entity.getName());
            ps.setString(3, String.valueOf(entity.getAge()));
            ps.setString(4, String.valueOf(entity.getCityId()));
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(final FootballTeamEntity entity) throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(UPDATE)) {
            ps.setString(1, entity.getName());
            ps.setString(2, String.valueOf(entity.getAge()));
            ps.setString(3, String.valueOf(entity.getCityId()));
            ps.setString(4, String.valueOf(entity.getId()));
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(final Integer id) throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(DELETE)) {
            ps.setString(1, String.valueOf(id));
            return ps.executeUpdate();
        }
    }
}
