package com.igor.dao.implementation;

import com.igor.dao.interfaces.FootballTeamHasCompetitionDAO;
import com.igor.model.entity.FootballTeamHasCompetitionEntity;
import com.igor.model.entity.PrimaryKeyFootballCompetition;
import com.igor.transformer.Transformer;

import java.lang.reflect.InvocationTargetException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static com.igor.model.constant.FootballTeamHasCompetitionConst.*;
import static com.igor.model.constant.GeneralConst.*;

public class FootballTeamHasCompetitionDaoImplementation implements FootballTeamHasCompetitionDAO {
    private static final String FIND_ALL = String.format(FIND_ALL_GENERAL, TABLE_NAME);
    private static final String DELETE = String.format("DELETE FROM %s WHERE %s=? AND %s=?",
            TABLE_NAME, FOOTBALL_TEAM_ID, COMPETITION_ID);
    private static final String CREATE = String.format("INSERT %s (%s, %s, %s) VALUES (?, ?, ?)",
            TABLE_NAME, FOOTBALL_TEAM_ID, COMPETITION_ID, AMOUNT_OF_WIN);
    private static final String UPDATE = String.format("UPDATE %s SET %s=? WHERE %s=? and %s=?",
            TABLE_NAME, AMOUNT_OF_WIN, FOOTBALL_TEAM_ID, COMPETITION_ID);
    private static final String FIND_BY_COMPETITION_ID = String.format(FIND_BY_ONE_COLUMN_GENERAL, TABLE_NAME, COMPETITION_ID);
    private static final String FIND_BY_FOOTBALL_TEAM_ID = String.format(FIND_BY_ONE_COLUMN_GENERAL, TABLE_NAME, FOOTBALL_TEAM_ID);
    private static final String FIND_BY_ID = String.format("SELECT * FROM %s WHERE %s=? AND %s=?",
            TABLE_NAME, FOOTBALL_TEAM_ID, COMPETITION_ID);
    private static final String FIND_BY_AMOUNT_OF_WIN = String.format(FIND_BY_ONE_COLUMN_GENERAL, TABLE_NAME, AMOUNT_OF_WIN);

    @Override
    public List<FootballTeamHasCompetitionEntity> findByCompetitionId(final int competitionId)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return findByNumberElement(competitionId, FIND_BY_COMPETITION_ID);
    }

    @Override
    public List<FootballTeamHasCompetitionEntity> findByFootballTeamId(final int footballTeamID)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return findByNumberElement(footballTeamID, FIND_BY_FOOTBALL_TEAM_ID);
    }

    @Override
    public List<FootballTeamHasCompetitionEntity> findByAmountOfWin(final int amountOfWin)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return findByNumberElement(amountOfWin, FIND_BY_AMOUNT_OF_WIN);
    }


    private List<FootballTeamHasCompetitionEntity> findByNumberElement(final Number element, final String findByNameInTable)
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        List<FootballTeamHasCompetitionEntity> footballTeams = new ArrayList<>();
        try (PreparedStatement ps = CONNECTION.prepareStatement(findByNameInTable)) {
            ps.setString(1, String.valueOf(element));
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    footballTeams.add((FootballTeamHasCompetitionEntity)
                            new Transformer<>(FootballTeamHasCompetitionEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return footballTeams;
    }

    @Override
    public List<FootballTeamHasCompetitionEntity> findAll()
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        List<FootballTeamHasCompetitionEntity> footballTeams = new ArrayList<>();
        try (Statement statement = CONNECTION.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    footballTeams.add((FootballTeamHasCompetitionEntity)
                            new Transformer<>(FootballTeamHasCompetitionEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return footballTeams;
    }


    @Override
    public FootballTeamHasCompetitionEntity findById(final PrimaryKeyFootballCompetition id)
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        FootballTeamHasCompetitionEntity entity = null;
        try (PreparedStatement ps = CONNECTION.prepareStatement(FIND_BY_ID)) {
            ps.setString(2, String.valueOf(id.getFootballTeamId()));
            ps.setString(1, String.valueOf(id.getCompetitionId()));
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    entity = (FootballTeamHasCompetitionEntity)
                            new Transformer<>(FootballTeamHasCompetitionEntity.class).fromResultSetToEntity(resultSet);
                }
            }
        }
        return entity;
    }

    @Override
    public int create(final FootballTeamHasCompetitionEntity entity) throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(CREATE)) {
            ps.setString(1, String.valueOf(entity.getPrimaryKey().getFootballTeamId()));
            ps.setString(2, String.valueOf(entity.getPrimaryKey().getCompetitionId()));
            ps.setString(3, String.valueOf(entity.getAmountOfWin()));
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(final FootballTeamHasCompetitionEntity entity) throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(UPDATE)) {
            ps.setString(1, String.valueOf(entity.getAmountOfWin()));
            ps.setString(2, String.valueOf(entity.getPrimaryKey().getFootballTeamId()));
            ps.setString(3, String.valueOf(entity.getPrimaryKey().getCompetitionId()));
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(final PrimaryKeyFootballCompetition id) throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(DELETE)) {
            ps.setString(1, String.valueOf(id.getFootballTeamId()));
            ps.setString(2, String.valueOf(id.getCompetitionId()));
            return ps.executeUpdate();
        }
    }
}
