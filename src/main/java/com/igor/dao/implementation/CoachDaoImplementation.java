package com.igor.dao.implementation;

import com.igor.dao.interfaces.CoachDAO;
import com.igor.model.entity.CoachEntity;
import com.igor.transformer.Transformer;

import java.lang.reflect.InvocationTargetException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static com.igor.model.constant.CoachConst.*;
import static com.igor.model.constant.GeneralConst.*;

public class CoachDaoImplementation implements CoachDAO {
    private static final String FIND_ALL = String.format(FIND_ALL_GENERAL, TABLE_NAME);
    private static final String DELETE = String.format(DELETE_GENERAL, TABLE_NAME, ID);
    private static final String CREATE = String.format("INSERT %s (%s, %s, %s, %s) VALUES (?, ?, ?, ?)",
            TABLE_NAME, ID, NAME, SALARY, FOOTBALL_TEAM_ID);
    private static final String UPDATE = String.format("UPDATE %s SET %s=?, %s=?, %s=? WHERE %s=?",
            TABLE_NAME, NAME, SALARY, FOOTBALL_TEAM_ID, ID);
    private static final String FIND_BY_ID = String.format(FIND_BY_ONE_COLUMN_GENERAL, TABLE_NAME, ID);
    private static final String FIND_BY_NAME = String.format(FIND_BY_ONE_COLUMN_GENERAL, TABLE_NAME, NAME);
    private static final String FIND_BY_SALARY = String.format(FIND_BY_ONE_COLUMN_GENERAL, TABLE_NAME, SALARY);
    private static final String FIND_BY_FOOTBALL_TEAM_ID = String.format(FIND_BY_ONE_COLUMN_GENERAL,
            TABLE_NAME, FOOTBALL_TEAM_ID);

    @Override
    public List<CoachEntity> findByName(final String name) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        List<CoachEntity> coaches = new ArrayList<>();
        try (PreparedStatement ps = CONNECTION.prepareStatement(FIND_BY_NAME)) {
            ps.setString(1, name);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    coaches.add((CoachEntity) new Transformer<>(CoachEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return coaches;
    }

    @Override
    public List<CoachEntity> findBySalary(final long salary) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        List<CoachEntity> coaches = new ArrayList<>();
        try (PreparedStatement ps = CONNECTION.prepareStatement(FIND_BY_SALARY)) {
            ps.setString(1, String.valueOf(salary));
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    coaches.add((CoachEntity) new Transformer<>(CoachEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return coaches;
    }

    @Override
    public CoachEntity findByFootballTeamId(final int footballTeamId) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        CoachEntity coach = null;
        try (PreparedStatement ps = CONNECTION.prepareStatement(FIND_BY_FOOTBALL_TEAM_ID)) {
            ps.setString(1, String.valueOf(footballTeamId));
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    coach = ((CoachEntity) new Transformer<>(CoachEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return coach;
    }

    @Override
    public List<CoachEntity> findAll() throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        List<CoachEntity> coaches = new ArrayList<>();
        try (Statement statement = CONNECTION.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    coaches.add((CoachEntity) new Transformer<>(CoachEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return coaches;
    }

    @Override
    public CoachEntity findById(final Integer id) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        CoachEntity entity = null;
        try (PreparedStatement ps = CONNECTION.prepareStatement(FIND_BY_ID)) {
            ps.setString(1, String.valueOf(id));
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    entity = (CoachEntity) new Transformer<>(CoachEntity.class).fromResultSetToEntity(resultSet);
                }
            }
        }
        return entity;
    }

    @Override
    public int create(final CoachEntity entity) throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(CREATE)) {
            ps.setInt(1, entity.getId());
            ps.setString(2, entity.getName());
            ps.setLong(3, entity.getSalary());
            ps.setInt(4, entity.getFootballTeamId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(final CoachEntity entity) throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(UPDATE)) {
            ps.setString(1, entity.getName());
            ps.setLong(2, entity.getSalary());
            ps.setInt(3, entity.getFootballTeamId());
            ps.setInt(4, entity.getId());
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(final Integer id) throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(DELETE)) {
            ps.setString(1, String.valueOf(id));
            return ps.executeUpdate();
        }
    }
}
