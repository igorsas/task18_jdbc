package com.igor.dao.implementation;

import com.igor.dao.interfaces.FootballerDAO;
import com.igor.model.entity.FootballerEntity;
import com.igor.transformer.Transformer;

import java.lang.reflect.InvocationTargetException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static com.igor.model.constant.FootballerConst.*;
import static com.igor.model.constant.GeneralConst.*;

public class FootballerDaoImplementation implements FootballerDAO {
    private static final String FIND_ALL = String.format(FIND_ALL_GENERAL, TABLE_NAME);
    private static final String DELETE = String.format(DELETE_GENERAL, TABLE_NAME, ID);
    private static final String CREATE = String.format("INSERT %s (%s, %s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?, ?)",
            TABLE_NAME, ID, FULL_NAME, AGE, MARKET_PRICE, SALARY, FOOTBALL_TEAM_ID, POSITION_ID);
    private static final String UPDATE = String.format("UPDATE %s SET %s=?, %s=?, %s=?, %s=?, %s=?, %s=? WHERE %s=?",
            TABLE_NAME, FULL_NAME, AGE, MARKET_PRICE, SALARY, FOOTBALL_TEAM_ID, POSITION_ID, ID);
    private static final String FIND_BY_ID = String.format(FIND_BY_ONE_COLUMN_GENERAL, TABLE_NAME, ID);
    private static final String FIND_BY_FULL_NAME = String.format(FIND_BY_ONE_COLUMN_GENERAL, TABLE_NAME, FULL_NAME);
    private static final String FIND_BY_AGE = String.format(FIND_BY_ONE_COLUMN_GENERAL, TABLE_NAME, AGE);
    private static final String FIND_BY_MARKET_PRICE = String.format(FIND_BY_ONE_COLUMN_GENERAL, TABLE_NAME, MARKET_PRICE);
    private static final String FIND_BY_SALARY = String.format(FIND_BY_ONE_COLUMN_GENERAL, TABLE_NAME, SALARY);
    private static final String FIND_BY_FOOTBALL_TEAM_ID = String.format(FIND_BY_ONE_COLUMN_GENERAL,
            TABLE_NAME, FOOTBALL_TEAM_ID);
    private static final String FIND_BY_POSITION_ID = String.format(FIND_BY_ONE_COLUMN_GENERAL, TABLE_NAME, POSITION_ID);

    @Override
    public List<FootballerEntity> findByFullName(final String name)
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        List<FootballerEntity> footballers = new ArrayList<>();
        try (PreparedStatement ps = CONNECTION.prepareStatement(FIND_BY_FULL_NAME)) {
            ps.setString(1, name);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    footballers.add((FootballerEntity) new Transformer<>(FootballerEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return footballers;
    }

    @Override
    public List<FootballerEntity> findByAge(final int age)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return findByNumberElement(age, FIND_BY_AGE);
    }

    @Override
    public List<FootballerEntity> findByMarketPrice(final long marketPrice)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return findByNumberElement(marketPrice, FIND_BY_MARKET_PRICE);
    }

    @Override
    public List<FootballerEntity> findBySalary(final long salary)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return findByNumberElement(salary, FIND_BY_SALARY);
    }

    @Override
    public List<FootballerEntity> findByFootballTeamId(final int footballTeamId)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return findByNumberElement(footballTeamId, FIND_BY_FOOTBALL_TEAM_ID);
    }

    @Override
    public List<FootballerEntity> findByPositionId(final int positionId)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return findByNumberElement(positionId, FIND_BY_POSITION_ID);
    }

    private List<FootballerEntity> findByNumberElement(final Number element, final String findByNameInTable)
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        List<FootballerEntity> footballers = new ArrayList<>();
        try (PreparedStatement ps = CONNECTION.prepareStatement(findByNameInTable)) {
            ps.setString(1, String.valueOf(element));
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    footballers.add((FootballerEntity) new Transformer<>(FootballerEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return footballers;
    }

    @Override
    public List<FootballerEntity> findAll()
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        List<FootballerEntity> footballers = new ArrayList<>();
        try (Statement statement = CONNECTION.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    footballers.add((FootballerEntity) new Transformer<>(FootballerEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return footballers;
    }


    @Override
    public FootballerEntity findById(final Integer id)
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        FootballerEntity entity = null;
        try (PreparedStatement ps = CONNECTION.prepareStatement(FIND_BY_ID)) {
            ps.setString(1, String.valueOf(id));
            try (ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    entity = (FootballerEntity) new Transformer<>(FootballerEntity.class).fromResultSetToEntity(resultSet);
                }
            }
        }
        return entity;
    }

    @Override
    public int create(final FootballerEntity entity) throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(CREATE)) {
            ps.setString(1, String.valueOf(entity.getId()));
            ps.setString(2, entity.getFullName());
            ps.setString(3, String.valueOf(entity.getAge()));
            ps.setString(4, String.valueOf(entity.getMarketPrice()));
            ps.setString(5, String.valueOf(entity.getSalary()));
            ps.setString(6, String.valueOf(entity.getFootballTeamId()));
            ps.setString(7, String.valueOf(entity.getPositionId()));
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(final FootballerEntity entity) throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(UPDATE)) {
            ps.setString(1, entity.getFullName());
            ps.setString(2, String.valueOf(entity.getAge()));
            ps.setString(3, String.valueOf(entity.getMarketPrice()));
            ps.setString(4, String.valueOf(entity.getSalary()));
            ps.setString(5, String.valueOf(entity.getFootballTeamId()));
            ps.setString(6, String.valueOf(entity.getPositionId()));
            ps.setString(7, String.valueOf(entity.getId()));
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(final Integer id) throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(DELETE)) {
            ps.setString(1, String.valueOf(id));
            return ps.executeUpdate();
        }
    }
}
