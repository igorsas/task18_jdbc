package com.igor.dao.implementation;

import com.igor.dao.interfaces.PositionDAO;
import com.igor.model.entity.PositionEntity;
import com.igor.transformer.Transformer;

import java.lang.reflect.InvocationTargetException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static com.igor.model.constant.GeneralConst.*;
import static com.igor.model.constant.PositionConst.*;

public class PositionDaoImplementation implements PositionDAO {
    private static final String FIND_ALL = String.format(FIND_ALL_GENERAL, TABLE_NAME);
    private static final String DELETE = String.format(DELETE_GENERAL, TABLE_NAME, ID);
    private static final String CREATE = String.format("INSERT %s (%s, %s) VALUES (?, ?)", TABLE_NAME, ID, NAME);
    private static final String UPDATE = String.format("UPDATE %s SET %s=? WHERE %s=?", TABLE_NAME, NAME, ID);
    private static final String FIND_BY_ID = String.format(FIND_BY_ONE_COLUMN_GENERAL, TABLE_NAME, ID);
    private static final String FIND_BY_NAME = String.format(FIND_BY_ONE_COLUMN_GENERAL, TABLE_NAME, NAME);

    @Override
    public List<PositionEntity> findByName(final String name)
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        List<PositionEntity> positions = new ArrayList<>();
        try (PreparedStatement ps = CONNECTION.prepareStatement(FIND_BY_NAME)) {
            ps.setString(1, name);
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    positions.add((PositionEntity) new Transformer<>(PositionEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return positions;
    }

    @Override
    public List<PositionEntity> findAll()
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        List<PositionEntity> positions = new ArrayList<>();
        try (Statement statement = CONNECTION.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
                while (resultSet.next()) {
                    positions.add((PositionEntity) new Transformer<>(PositionEntity.class).fromResultSetToEntity(resultSet));
                }
            }
        }
        return positions;
    }

    @Override
    public PositionEntity findById(final Integer id)
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        PositionEntity entity = null;
        try (PreparedStatement ps = CONNECTION.prepareStatement(FIND_BY_ID)) {
            ps.setString(1, String.valueOf(id));
            try (ResultSet resultSet = ps.executeQuery()) {
                while (resultSet.next()) {
                    entity = (PositionEntity) new Transformer<>(PositionEntity.class).fromResultSetToEntity(resultSet);
                }
            }
        }
        return entity;
    }

    @Override
    public int create(final PositionEntity entity) throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(CREATE)) {
            ps.setString(NUMBER_ID_IN_TABLE, String.valueOf(entity.getId()));
            ps.setString(NUMBER_NAME_IN_TABLE, entity.getName());
            return ps.executeUpdate();
        }
    }

    @Override
    public int update(final PositionEntity entity) throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(UPDATE)) {
            ps.setString(1, entity.getName());
            ps.setString(2, String.valueOf(entity.getId()));
            return ps.executeUpdate();
        }
    }

    @Override
    public int delete(final Integer id) throws SQLException {
        try (PreparedStatement ps = CONNECTION.prepareStatement(DELETE)) {
            ps.setString(NUMBER_ID_IN_TABLE, String.valueOf(id));
            return ps.executeUpdate();
        }
    }
}
