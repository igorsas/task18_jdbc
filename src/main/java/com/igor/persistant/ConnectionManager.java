package com.igor.persistant;

import com.igor.model.property.Property;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Objects;

import static com.igor.model.constant.PropertyConst.*;
import static com.igor.view.View.LOG;

public final class ConnectionManager {
    private static final String url = Property.getProperty(URL_KEY);
    private static final String user = Property.getProperty(USER_KEY);
    private static final String password = Property.getProperty(PASSWORD_KEY);
    private static Connection connection;

    private ConnectionManager() {
    }

    public static Connection getConnection() {
        if (Objects.isNull(connection)) {
            try {
                connection = DriverManager.getConnection(Objects.requireNonNull(url), user, password);
            } catch (SQLException e) {
                LOG.error(e.getMessage());
                LOG.error(e.getSQLState());
                LOG.error(e.getErrorCode());
            }
        }
        return connection;
    }
}
