package com.igor.service.interfaces;

import com.igor.model.entity.CoachEntity;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public interface CoachService extends GeneralService<CoachEntity, Integer> {
    List<CoachEntity> findByName(String name) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;

    List<CoachEntity> findBySalary(long salary) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;

    CoachEntity findByFootballTeamId(int footballTeamId) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;
}
