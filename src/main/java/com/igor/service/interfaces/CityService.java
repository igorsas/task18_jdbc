package com.igor.service.interfaces;

import com.igor.model.entity.CityEntity;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public interface CityService extends GeneralService<CityEntity, Integer> {
    List<CityEntity> findByName(String name) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;
}
