package com.igor.service.interfaces;

import com.igor.model.entity.FootballTeamHasCompetitionEntity;
import com.igor.model.entity.PrimaryKeyFootballCompetition;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public interface FootballTeamHasCompetitionService extends GeneralService<FootballTeamHasCompetitionEntity, PrimaryKeyFootballCompetition> {
    List<FootballTeamHasCompetitionEntity> findByCompetitionId(int competitionId) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;

    List<FootballTeamHasCompetitionEntity> findByFootballTeamId(int competitionId) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;

    List<FootballTeamHasCompetitionEntity> findByAmountOfWin(int amountOfWin) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;
}
