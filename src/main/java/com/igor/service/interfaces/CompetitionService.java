package com.igor.service.interfaces;

import com.igor.model.entity.CompetitionEntity;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public interface CompetitionService extends GeneralService<CompetitionEntity, Integer> {
    List<CompetitionEntity> findByName(String name) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;

    List<CompetitionEntity> findByPrizeFund(long prizeFund) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;
}
