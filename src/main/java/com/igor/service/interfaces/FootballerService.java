package com.igor.service.interfaces;

import com.igor.model.entity.FootballerEntity;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public interface FootballerService extends GeneralService<FootballerEntity, Integer> {
    List<FootballerEntity> findByFullName(String fullName) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;

    List<FootballerEntity> findByAge(int age) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;

    List<FootballerEntity> findByMarketPrice(long marketPrice) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;

    List<FootballerEntity> findBySalary(long salary) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;

    List<FootballerEntity> findByFootballTeamId(int footballTeamId) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;

    List<FootballerEntity> findByPositionId(int positionId) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;
}
