package com.igor.service.interfaces;

import com.igor.model.entity.FootballTeamEntity;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public interface FootballTeamService extends GeneralService<FootballTeamEntity, Integer> {
    List<FootballTeamEntity> findByName(String name) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;

    List<FootballTeamEntity> findByAge(int age) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;

    List<FootballTeamEntity> findByCityId(int cityId) throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException;
}
