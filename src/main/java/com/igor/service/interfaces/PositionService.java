package com.igor.service.interfaces;

import com.igor.model.entity.PositionEntity;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public interface PositionService extends GeneralService<PositionEntity, Integer> {
    List<PositionEntity> findByName(String name) throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException;
}
