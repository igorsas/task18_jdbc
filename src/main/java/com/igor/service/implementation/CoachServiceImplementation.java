package com.igor.service.implementation;

import com.igor.dao.implementation.CoachDaoImplementation;
import com.igor.model.entity.CoachEntity;
import com.igor.service.interfaces.CoachService;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public class CoachServiceImplementation implements CoachService {
    @Override
    public List<CoachEntity> findByName(final String name)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return new CoachDaoImplementation().findByName(name);
    }

    @Override
    public List<CoachEntity> findBySalary(final long salary)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return new CoachDaoImplementation().findBySalary(salary);
    }

    @Override
    public CoachEntity findByFootballTeamId(final int footballTeamId)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return new CoachDaoImplementation().findByFootballTeamId(footballTeamId);
    }

    @Override
    public List<CoachEntity> findAll()
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return new CoachDaoImplementation().findAll();
    }


    @Override
    public CoachEntity findById(final Integer id)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return new CoachDaoImplementation().findById(id);
    }

    @Override
    public int create(final CoachEntity entity) throws SQLException {
        return new CoachDaoImplementation().create(entity);
    }

    @Override
    public int update(final CoachEntity entity) throws SQLException {
        return new CoachDaoImplementation().update(entity);
    }

    @Override
    public int delete(final Integer id) throws SQLException {
        return new CoachDaoImplementation().delete(id);
    }
}
