package com.igor.service.implementation;

import com.igor.dao.implementation.CityDaoImplementation;
import com.igor.model.entity.CityEntity;
import com.igor.service.interfaces.CityService;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public class CityServiceImplementation implements CityService {
    @Override
    public List<CityEntity> findByName(final String name)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return new CityDaoImplementation().findByName(name);
    }

    @Override
    public List<CityEntity> findAll()
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return new CityDaoImplementation().findAll();
    }

    @Override
    public CityEntity findById(final Integer id)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return new CityDaoImplementation().findById(id);
    }

    @Override
    public int create(final CityEntity entity) throws SQLException {
        return new CityDaoImplementation().create(entity);
    }

    @Override
    public int update(final CityEntity entity) throws SQLException {
        return new CityDaoImplementation().update(entity);
    }

    @Override
    public int delete(final Integer id) throws SQLException {
        return new CityDaoImplementation().delete(id);
    }
}
