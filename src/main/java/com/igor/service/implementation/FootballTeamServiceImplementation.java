package com.igor.service.implementation;

import com.igor.dao.implementation.FootballTeamDaoImplementation;
import com.igor.model.entity.FootballTeamEntity;
import com.igor.service.interfaces.FootballTeamService;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public class FootballTeamServiceImplementation implements FootballTeamService {
    @Override
    public List<FootballTeamEntity> findByName(final String name)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return new FootballTeamDaoImplementation().findByName(name);
    }

    @Override
    public List<FootballTeamEntity> findByAge(final int age)
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        return new FootballTeamDaoImplementation().findByAge(age);
    }

    @Override
    public List<FootballTeamEntity> findByCityId(final int id)
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        return new FootballTeamDaoImplementation().findByCityId(id);
    }

    @Override
    public List<FootballTeamEntity> findAll()
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return new FootballTeamDaoImplementation().findAll();
    }


    @Override
    public FootballTeamEntity findById(final Integer id)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return new FootballTeamDaoImplementation().findById(id);
    }

    @Override
    public int create(final FootballTeamEntity entity) throws SQLException {
        return new FootballTeamDaoImplementation().create(entity);
    }

    @Override
    public int update(final FootballTeamEntity entity) throws SQLException {
        return new FootballTeamDaoImplementation().update(entity);
    }

    @Override
    public int delete(final Integer id) throws SQLException {
        return new FootballTeamDaoImplementation().delete(id);
    }
}
