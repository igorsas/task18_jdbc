package com.igor.service.implementation;

import com.igor.dao.implementation.FootballerDaoImplementation;
import com.igor.model.entity.FootballerEntity;
import com.igor.service.interfaces.FootballerService;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public class FootballerServiceImplementation implements FootballerService {
    @Override
    public List<FootballerEntity> findByFullName(final String name)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return new FootballerDaoImplementation().findByFullName(name);
    }

    @Override
    public List<FootballerEntity> findByAge(final int age)
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        return new FootballerDaoImplementation().findByAge(age);
    }

    @Override
    public List<FootballerEntity> findByMarketPrice(final long marketPrice)
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        return new FootballerDaoImplementation().findByMarketPrice(marketPrice);
    }

    @Override
    public List<FootballerEntity> findBySalary(final long salary)
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        return new FootballerDaoImplementation().findBySalary(salary);
    }

    @Override
    public List<FootballerEntity> findByFootballTeamId(final int footballTeamId)
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        return new FootballerDaoImplementation().findByFootballTeamId(footballTeamId);
    }

    @Override
    public List<FootballerEntity> findByPositionId(final int positionId)
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        return new FootballerDaoImplementation().findByPositionId(positionId);
    }

    @Override
    public List<FootballerEntity> findAll()
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return new FootballerDaoImplementation().findAll();
    }


    @Override
    public FootballerEntity findById(final Integer id)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return new FootballerDaoImplementation().findById(id);
    }

    @Override
    public int create(final FootballerEntity entity) throws SQLException {
        return new FootballerDaoImplementation().create(entity);
    }

    @Override
    public int update(final FootballerEntity entity) throws SQLException {
        return new FootballerDaoImplementation().update(entity);
    }

    @Override
    public int delete(final Integer id) throws SQLException {
        return new FootballerDaoImplementation().delete(id);
    }
}
