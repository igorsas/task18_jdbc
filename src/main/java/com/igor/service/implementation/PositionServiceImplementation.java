package com.igor.service.implementation;

import com.igor.dao.implementation.PositionDaoImplementation;
import com.igor.model.entity.PositionEntity;
import com.igor.service.interfaces.PositionService;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public class PositionServiceImplementation implements PositionService {
    @Override
    public List<PositionEntity> findByName(final String name)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return new PositionDaoImplementation().findByName(name);
    }

    @Override
    public List<PositionEntity> findAll()
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return new PositionDaoImplementation().findAll();
    }

    @Override
    public PositionEntity findById(final Integer id)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return new PositionDaoImplementation().findById(id);
    }

    @Override
    public int create(final PositionEntity entity) throws SQLException {
        return new PositionDaoImplementation().create(entity);
    }

    @Override
    public int update(final PositionEntity entity) throws SQLException {
        return new PositionDaoImplementation().update(entity);
    }

    @Override
    public int delete(final Integer id) throws SQLException {
        return new PositionDaoImplementation().delete(id);
    }
}
