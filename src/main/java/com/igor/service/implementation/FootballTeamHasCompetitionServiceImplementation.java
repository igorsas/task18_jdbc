package com.igor.service.implementation;

import com.igor.dao.implementation.FootballTeamHasCompetitionDaoImplementation;
import com.igor.model.entity.FootballTeamHasCompetitionEntity;
import com.igor.model.entity.PrimaryKeyFootballCompetition;
import com.igor.service.interfaces.FootballTeamHasCompetitionService;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public class FootballTeamHasCompetitionServiceImplementation implements FootballTeamHasCompetitionService {
    @Override
    public List<FootballTeamHasCompetitionEntity> findByCompetitionId(final int competitionId)
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        return new FootballTeamHasCompetitionDaoImplementation().findByCompetitionId(competitionId);
    }

    @Override
    public List<FootballTeamHasCompetitionEntity> findByFootballTeamId(final int footballTeamID)
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        return new FootballTeamHasCompetitionDaoImplementation().findByFootballTeamId(footballTeamID);
    }

    @Override
    public List<FootballTeamHasCompetitionEntity> findByAmountOfWin(final int amountOfWin)
            throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        return new FootballTeamHasCompetitionDaoImplementation().findByAmountOfWin(amountOfWin);
    }

    @Override
    public List<FootballTeamHasCompetitionEntity> findAll()
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return new FootballTeamHasCompetitionDaoImplementation().findAll();
    }


    @Override
    public FootballTeamHasCompetitionEntity findById(final PrimaryKeyFootballCompetition id)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return new FootballTeamHasCompetitionDaoImplementation().findById(id);
    }

    @Override
    public int create(final FootballTeamHasCompetitionEntity entity) throws SQLException {
        return new FootballTeamHasCompetitionDaoImplementation().create(entity);
    }

    @Override
    public int update(final FootballTeamHasCompetitionEntity entity) throws SQLException {
        return new FootballTeamHasCompetitionDaoImplementation().update(entity);
    }

    @Override
    public int delete(final PrimaryKeyFootballCompetition id) throws SQLException {
        return new FootballTeamHasCompetitionDaoImplementation().delete(id);
    }
}
