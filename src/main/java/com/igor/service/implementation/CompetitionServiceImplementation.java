package com.igor.service.implementation;

import com.igor.dao.implementation.CompetitionDaoImplementation;
import com.igor.model.entity.CompetitionEntity;
import com.igor.service.interfaces.CompetitionService;

import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public class CompetitionServiceImplementation implements CompetitionService {
    @Override
    public List<CompetitionEntity> findByName(final String name)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return new CompetitionDaoImplementation().findByName(name);
    }

    @Override
    public List<CompetitionEntity> findByPrizeFund(final long prizeFund)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return new CompetitionDaoImplementation().findByPrizeFund(prizeFund);
    }

    @Override
    public List<CompetitionEntity> findAll()
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return new CompetitionDaoImplementation().findAll();
    }


    @Override
    public CompetitionEntity findById(final Integer id)
            throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        return new CompetitionDaoImplementation().findById(id);
    }

    @Override
    public int create(final CompetitionEntity entity) throws SQLException {
        return new CompetitionDaoImplementation().create(entity);
    }

    @Override
    public int update(final CompetitionEntity entity) throws SQLException {
        return new CompetitionDaoImplementation().update(entity);
    }

    @Override
    public int delete(final Integer id) throws SQLException {
        return new CompetitionDaoImplementation().delete(id);
    }
}
