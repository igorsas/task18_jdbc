package com.igor.view;

import com.igor.controller.*;
import com.igor.service.implementation.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.igor.model.constant.GeneralConst.*;
import static com.igor.model.constant.ViewConst.*;

public class View {
    public static final Logger LOG = LogManager.getLogger(View.class);
    private Map<String, String> menu;
    private Map<String, Controller> methodsMenu;

    public View() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();
        putInstructions();
        pubRealizations();
    }

    private static void outputMenu(final Map<String, String> menu) {
        LOG.info(MENU_STR);
        for (String key : menu.keySet()) {
            LOG.info(menu.get(key));
        }
    }

    public static void showMenu(final Map<String, String> menu, final Map<String, Controller> methodsMenu) {
        String keyMenu;
        do {
            outputMenu(menu);
            LOG.info(SELECT_MENU_POINT);
            try {
                keyMenu = INPUT.readLine().toUpperCase();
                if (keyMenu.equals(SIGN_QUIT)) {
                    break;
                }
                methodsMenu.get(keyMenu).execute();
            } catch (InstantiationException | InvocationTargetException |
                    NoSuchMethodException | IllegalAccessException | NullPointerException | IOException e) {
                LOG.error(e.getClass());
                LOG.error(e.getMessage());
                LOG.trace(e.getStackTrace());
            } catch (SQLException e) {
                LOG.error(e.getClass());
                LOG.error(e.getMessage());
                LOG.trace(e.getStackTrace());
                LOG.error(e.getSQLState());
                LOG.error(e.getErrorCode());
            }
        } while (true);
    }

    public void show() {
        showMenu(menu, methodsMenu);
    }

    private void putInstructions() {
        int point = STARTED_POINT;
        menu.put(SIGN_SELECT_ALL_TABLE, TAB + SIGN_SELECT_ALL_TABLE + SELECT_ALL_TABLE);
        menu.put(SIGN_SELECT_DB_STRUCTURE, TAB + SIGN_SELECT_DB_STRUCTURE + SELECT_DB_STRUCTURE);
        putInstruction(point++, CITY);
        putInstruction(point++, COACH);
        putInstruction(point++, COMPETITION);
        putInstruction(point++, FOOTBALLER);
        putInstruction(point++, FOOTBALL_TEAM);
        putInstruction(point++, FOOTBALL_TEAM_HAS_COMPETITION);
        putInstruction(point, POSITION);
        menu.put(SIGN_QUIT, EXIT_MSG);
    }

    private void putInstruction(final int point, final String table) {
        menu.put(String.valueOf(point), String.format(PUT_TABLE_IN_MAP, point, table));
    }

    private void pubRealizations() {
        int point = STARTED_POINT;
        methodsMenu.put(SIGN_SELECT_ALL_TABLE, this::selectAllTable);
        methodsMenu.put(SIGN_SELECT_DB_STRUCTURE, this::takeStructureOfDB);
        methodsMenu.put(String.valueOf(point++), new CityController());
        methodsMenu.put(String.valueOf(point++), new CoachController());
        methodsMenu.put(String.valueOf(point++), new CompetitionController());
        methodsMenu.put(String.valueOf(point++), new FootballerController());
        methodsMenu.put(String.valueOf(point++), new FootballTeamController());
        methodsMenu.put(String.valueOf(point++), new FootballTeamHasCompetitionController());
        methodsMenu.put(String.valueOf(point), new PositionController());
    }

    private void takeStructureOfDB() throws SQLException {
        MetaDataService metaDataService = new MetaDataService();
        LOG.info(TABLE_STR + OF_DATABASE_STR + CONNECTION.getCatalog());
        metaDataService.getTablesStructure().forEach(LOG::info);
    }

    private void selectAllTable() throws SQLException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        selectCity();
        selectCoach();
        selectCompetition();
        selectFootballer();
        selectFootballHasCompetition();
        selectFootballTeam();
        selectPosition();
    }

    private void selectPosition() throws SQLException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        LOG.info(TABLE_STR + POSITION);
        new PositionServiceImplementation().findAll().forEach(LOG::info);
    }

    private void selectFootballTeam() throws SQLException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        LOG.info(TABLE_STR + FOOTBALL_TEAM);
        new FootballTeamServiceImplementation().findAll().forEach(LOG::info);
    }

    private void selectFootballHasCompetition() throws SQLException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        LOG.info(TABLE_STR + FOOTBALL_TEAM_HAS_COMPETITION);
        new FootballTeamHasCompetitionServiceImplementation().findAll().forEach(LOG::info);
    }

    private void selectFootballer() throws SQLException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        LOG.info(TABLE_STR + FOOTBALLER);
        new FootballerServiceImplementation().findAll().forEach(LOG::info);
    }

    private void selectCompetition() throws SQLException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        LOG.info(TABLE_STR + COMPETITION);
        new CompetitionServiceImplementation().findAll().forEach(LOG::info);
    }

    private void selectCoach() throws SQLException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        LOG.info(TABLE_STR + COACH);
        new CoachServiceImplementation().findAll().forEach(LOG::info);
    }

    private void selectCity() throws SQLException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        LOG.info(TABLE_STR + CITY);
        new CityServiceImplementation().findAll().forEach(LOG::info);
    }
}
