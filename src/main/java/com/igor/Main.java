package com.igor;

import com.igor.view.View;

public class Main {
    public static void main(final String[] args) {
        new View().show();
    }
}
